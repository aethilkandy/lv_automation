package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for albertsons Homepage functionalities
*/
import static org.testng.Assert.assertTrue;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetPageSource;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.Test;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;

public class LouisvuittonHome extends ActionValidationLibrary {
    private static LouisvuittonHome louisvuittonHomePage;

    public static Logger logger = Logger.getLogger(LouisvuittonHome.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Home Page Class
     *****************************************************************************************************************************************/
    
    By headerName = By.xpath("//h1[text()='LOUIS VUITTON']");
    
    By tablvNow = By.xpath("//span[text()='LV Now']");
    
    By tabWorldLV = By.xpath("//button[text()='World of Louis Vuitton']");
    
    By tabWomen =By.xpath("//button[text()='Women']");
    
    By tabMen = By.xpath("//button[text()='Men']");
    
    By tabStores = By.xpath("//span[text()='Stores']");
    
    By searchLevelOne = By.xpath("//li[@class='level1 search ']//div[@role='search']//*[@class='ui-icon-navigation-search']//*[@fill-rule='evenodd']");
    
    By cart = By.xpath("//*[@class='ui-icon-navigation-cart']//*[@fill-rule='evenodd']");
    
    By wishList = By.xpath("//*[@class='ui-icon-navigation-wishlist-off']//*[@fill-rule='evenodd']");
    
    By MyLv =By.xpath("//*[@class='ui-icon-navigation-my-lv']//*[@fill-rule='evenodd']");
    
    By help =By.xpath("//*[@class='ui-icon-navigation-help']//*[@fill-rule='evenodd']");
    
    By txtCountryName =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[1]");
    
    By footerNewsLetter =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[2]");

    By footerContact =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[3]");
    
    By footerApps =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[4]");
    
    By footerFollowUs =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[5]");
    
    By footerLegalPrivacyLnk =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[6]");

    By footerJobsLnk =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[7]");

    By footerSiteMap =By.xpath("//div[@class='footer-menu-wrapper']//ul//li[8]");
    
    By btnAcceptAndContinue = By.xpath("//span[text()='Accept and Continue']");
    
    By lnkFollowUs = By.xpath("//ul[@class='follow-us']");
    
    By lnkSiteMap = By.xpath("//div[text()='SITEMAP']");
    
    By searchButton = By.xpath("//li[@class='level1 search ']//button[@aria-label='Search']//*[@class='ui-icon-navigation-search']");

    By searchLabel = By.xpath("//input[@id='searchHeaderInput']");
    
    By searchLens =By.xpath("//button[@name='searchOkButton']");
    
    By addToCart = By.xpath("//button[@id='addToCartSubmit']");
    
    By countCart = By.xpath("//div[@class='count countCart']");
    
    By btnShoppingBag = By.xpath("//button[@id='header-shoppingBag']");
    
    By btnAddToCart = By.xpath("//button[@id='addToCartSubmit']");
    
    By txtYourShoppingBag = By.xpath("//h1[@class='shopping-bag-title']");
    
    By btnViewCartDetails = By.xpath("//a[@id='viewTaxesAndDetails']");
    
    By btnSubmit = By.xpath("//button[@id='proceedToCheckoutButtonTop']");
    
    By btnMyLv = By.xpath("//li[@id='header-mylv']//button[@data-evt-action-id='my_lv']");
    
    By txtLogin = By.xpath("//label[@for='loginmylv-bubbleloginFormmylv-bubble']");
    
    By btnCreatAcnt = By.xpath("//span[@id='createAccount']");
    
    By chkSize= By.xpath("//select[@id='size']");
    
    By valueSize = By.xpath("//select[@id='size']//option[2]");
    
    By btnCart = By.xpath("//button[@id='header-shoppingBag']");
    
    By btnAddToShoppingBag = By.xpath("//button[@id='addToCartSubmit']");
    
    By stripStyle = By.xpath("//div[@class='fc-swatch-group']");
    
    By bagColors = By.xpath("//div[@class='fc-nav-flyout-ag-cas-wrapper']//div[@aria-label='Select Two Colors']");
    
    By colorValue = By.xpath("//span[@aria-label='Fuchsia']");
    
    By interiorColorValue = By.xpath("//span[@aria-label='Armagnac']");
    
    By interiorColor = By.xpath("//div[@class='fc-nav-flyout-ag-cas-wrapper']//div[@aria-label='Pick One Interior Color']");
    
    By interiorValue = By.xpath("//div[@class='fc-nav-flyout-ag-cas-wrapper']//div[@aria-label='Pick One Interior Color']");
    
    By addYourInitials = By.xpath("//div[@class='fc-nav-flyout-ag-cas-wrapper']//div[@aria-label='Add your Initials']");
    
    By txtInitials = By.xpath("//input[@aria-label='Initials']");
    
    By btnPlceInCart = By.xpath("//div[@aria-label='Place in Cart']");
    
    By txtMailId = By.xpath("//div[@id='loginPlaceholder_mylv-bubble']//div[@class='sign-in-mode']//form//div[@class='loginFormBox']//div[@class='formPattern4']//div[@id='fl_loginmylv-bubbleloginFormmylv-bubble']//div[@class='inputColumn']//div[@class='inputColumnTable']//div[@class='inputColumnRow']//input[@type='email']");
    
    By txtPassword = By.xpath("//div[@id='loginPlaceholder_mylv-bubble']//div[@class='sign-in-mode']//form//div[@class='loginFormBox']//div[@class='formPattern4']//div[@id='fl_passwordloginFormmylv-bubble']//div[@class='inputColumn']//div[@class='inputColumnTable']//div[@class='inputColumnRow']//div[@class='psw_c']//input[@type='password']");
    
    By btnSignIn = By.xpath("//div[@class='actions sign-in-mode button-block']//button//span[text()='Sign in']");
     
    By btnMyAccount = By.xpath("//a[@data-evt-content-id='my_profil']");
    
   private LouisvuittonHome() {

    }
   
   public static LouisvuittonHome getLouisvuittonHomePage() {

        if (LouisvuittonHome.louisvuittonHomePage == null) {
            LouisvuittonHome.louisvuittonHomePage = new LouisvuittonHome();
        }
        return LouisvuittonHome.louisvuittonHomePage;
    }
    
      
    public void navigateToLouisvuittonHome() throws Exception {
    	String URL=null;
    	try {
    		wait(6);
    		Runtime.getRuntime().exec("C:\\Users\\apattali\\Desktop\\stage\\AtuoIT.exe");
    		driver.get("https://Vuitton:5xaQqMK854@en-ppe.louisvuitton.com/eng-nl/homepage?dispatchCountry=BE");
    		wait(3);
    		this.waitForElementToBeVisible(this.btnAcceptAndContinue, "Accept and continue");
    		this.click(this.btnAcceptAndContinue, "Accept and continue");
    		final boolean flagHeader = Base.driver.findElement(this.headerName).isDisplayed();
    		assertTrue(flagHeader);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Louisvuitton Home" + e.getMessage());
            throw e;
        }
    }
    
    public void verifyMyLvButton() throws Exception {
    	try {
    		this.waitForElementToBePresent(btnMyLv, "My LV");
    		this.click(this.btnMyLv, "My LV");
    		final boolean flagUserName = Base.driver.findElement(this.txtLogin).isDisplayed();
    		assertTrue(flagUserName);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load MyLV page" + e.getMessage());
            throw e;
        }
    }
    
    public void verifySignIn() throws Exception {
    	try {
    		this.waitForElementToBePresent(txtMailId,"MailID");
    		driver.findElement(this.txtMailId).click();
    		driver.findElement(this.txtMailId).sendKeys("aamypattali@gmail.com");
    		
    		this.waitForElementToBePresent(txtPassword,"Password");
    		driver.findElement(this.txtPassword).sendKeys("Aamy@1995");
    		
    		this.waitForElementToBePresent(btnSignIn,"SignIn");
    		this.click(this.btnSignIn, "SignIn");
    		
    		this.waitForElementToBePresent(btnMyAccount, "MyAccount");
    		final boolean flagMyAccont = Base.driver.findElement(this.btnMyAccount).isDisplayed();
    		assertTrue(flagMyAccont);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load MyAccont page" + e.getMessage());
            throw e;
        }
    }
   
    public void aboutNewsletterLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.footerNewsLetter, "Newsletter");
	        final boolean flagNewsletter = Base.driver.findElement(this.footerNewsLetter).isDisplayed();
	        assertTrue(flagNewsletter);	
	        this.click(this.footerNewsLetter, "Newsletter");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String urlNewsletter = null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","aboutNewsletterLinkVerification").get(0).get("NEWSLETTER_URL")!=null){
    			urlNewsletter =(String)dataloader.getTestScenarios("LOUISVUITTON.json","aboutNewsletterLinkVerification").get(0).get("NEWSLETTER_URL");
    			Assert.assertEquals(Base.driver.getCurrentUrl(),urlNewsletter);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void contactLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.footerContact, "Contact Link");
	        final boolean flagContact = Base.driver.findElement(this.footerContact).isDisplayed();
	        assertTrue(flagContact);	
	        this.click(this.footerContact, "Contact");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String urlContact = null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","contactLinkVerification").get(0).get("CONTACT_URL")!=null){
    			urlContact =(String)dataloader.getTestScenarios("LOUISVUITTON.json","contactLinkVerification").get(0).get("CONTACT_URL");
    			Assert.assertEquals(Base.driver.getCurrentUrl(),urlContact);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load contact page" + e.getMessage());
            throw e;
        }
    }
    
    public void appsLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.footerApps, "Apps Link");
	        final boolean flagApps = Base.driver.findElement(this.footerApps).isDisplayed();
	        assertTrue(flagApps);	
	        this.click(this.footerApps, "Apps");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String urlApps = null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","appsLinkVerification").get(0).get("APPS_URL")!=null){
    			urlApps =(String)dataloader.getTestScenarios("LOUISVUITTON.json","appsLinkVerification").get(0).get("APPS_URL");
    			Assert.assertEquals(Base.driver.getCurrentUrl(),urlApps);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load contact page" + e.getMessage());
            throw e;
        }
    }
    
    public void followUsLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.footerFollowUs, "FollowUs Link");
	        final boolean flagFollowUs = Base.driver.findElement(this.footerFollowUs).isDisplayed();
	        assertTrue(flagFollowUs);	
	        this.click(this.footerFollowUs, "FollowUs Link");
	        wait(1);
	        boolean flagFollowUsLnk = Base.driver.findElement(this.lnkFollowUs).isDisplayed();
	        assertTrue(flagFollowUsLnk);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load contact page" + e.getMessage());
            throw e;
        }
    }
    
    public void legalAndPrivacyLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.footerLegalPrivacyLnk, "Legal and Privacy Link");
	        final boolean flagLegalAndPrivacy = Base.driver.findElement(this.footerLegalPrivacyLnk).isDisplayed();
	        assertTrue(flagLegalAndPrivacy);	
	        this.click(this.footerLegalPrivacyLnk, "Legal and Privacy Link");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String urlLegalPrivacy = null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","legalAndPrivacyLinkVerification").get(0).get("LEGAL_PRIVACY_URL")!=null){
    			urlLegalPrivacy =(String)dataloader.getTestScenarios("LOUISVUITTON.json","legalAndPrivacyLinkVerification").get(0).get("LEGAL_PRIVACY_URL");
    			Assert.assertEquals(Base.driver.getCurrentUrl(),urlLegalPrivacy);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load leagal privacy link" + e.getMessage());
            throw e;
        }
    }
    
    public void jobsLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.footerJobsLnk, "Jobs Link");
	        final boolean flagJobs = Base.driver.findElement(this.footerJobsLnk).isDisplayed();
	        assertTrue(flagJobs);	
	        this.click(this.footerJobsLnk, "Jobs Link");
	        String winHandleBefore = driver.getWindowHandle();
	        for(String winHandle : driver.getWindowHandles()){
	            driver.switchTo().window(winHandle);
	        }
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String urlJobs = null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","jobsLinkVerification").get(0).get("JOBS_URL")!=null){
    			urlJobs =(String)dataloader.getTestScenarios("LOUISVUITTON.json","jobsLinkVerification").get(0).get("JOBS_URL");
    			Assert.assertEquals(Base.driver.getCurrentUrl(),urlJobs);
    			driver.close();
    			driver.switchTo().window(winHandleBefore);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load jobs link" + e.getMessage());
            throw e;
        }
    }
    
    public void siteMapLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.footerSiteMap, "SiteMap Link");
	        final boolean flagSiteMap = Base.driver.findElement(this.footerSiteMap).isDisplayed();
	        assertTrue(flagSiteMap);	
	        this.click(this.footerSiteMap, "SiteMap Link");
	        wait(1);
	        boolean flagSiteMapLnk = Base.driver.findElement(this.lnkSiteMap).isDisplayed();
	        assertTrue(flagSiteMapLnk);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to load SiteMaps link" + e.getMessage());
            throw e;
        }
    }
    
    public void verifySearchAndFilter(String  product) throws Exception {
    	try {
    		wait(10);
    		String searchKey=null;
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifySearchAndFilter").get(0).get("Search_Value")!=null){
    			searchKey  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifySearchAndFilter").get(0).get("Search_Value");
    			
    		}
    		this.waitForElementToBePresent(searchButton, "Search Button");
    		this.click(this.searchButton, "Search Button");
    		this.waitForElementToBePresent(this.searchLabel, "Search");
    		if(product=="M41414"){
			driver.findElement(this.searchLabel).sendKeys("M41414");
    		}else if(product=="LP0001"){
    			driver.findElement(this.searchLabel).sendKeys("LP0001");
    		}else if(product=="004940"){
    			driver.findElement(this.searchLabel).sendKeys("004940");
    		}
			wait(5);
			final boolean flagSearchLens = Base.driver.findElement(this.searchLens).isDisplayed();
			assertTrue(flagSearchLens);
			this.click(this.searchLens, "Search Lens");
			wait(2);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Product details is not visible" + e.getMessage());
            throw e;
        }
    }
    
    public void addToCart(boolean flag) throws Exception {
    	try {
    		if(flag==true){
    		this.waitForElementToBePresent(addToCart, "Add to cart");
    		this.click(this.addToCart, "Add to cart");
    		}
    		wait(9);
    		this.waitForElementToBePresent(this.countCart, "Cart count");
			int count = Integer.parseInt(driver.findElement(this.countCart).getText());
			assertTrue(count>0);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Add to cart is not visible" + e.getMessage());
            throw e;
        }
    }
    
    public void selectProperties() throws Exception {
    	try {
    		this.click(this.stripStyle, "Strip Style");
    		waitForElementToBeClickable(this.bagColors, "bagColors");
    		this.click(this.bagColors, "bagColors");
    		waitForElementToBeClickable(this.colorValue, "colorValue");
    		this.click(this.bagColors, "colorValue");
    		waitForElementToBeClickable(this.interiorColor, "Interior color");
    		this.click(this.interiorColor, "Interior color");
    		wait(2);
    		
    		waitForElementToBeClickable(this.interiorColorValue, "interiorColorValue");
    		this.click(this.interiorColorValue, "interiorColorValue");
    		waitForElementToBeClickable(this.addYourInitials, "Add your initials");
    		this.click(this.addYourInitials, "Add your initials");
    		driver.findElement(this.txtInitials).sendKeys("111");
    		waitForElementToBeClickable(this.btnPlceInCart, "Place in cart");
    		this.click(this.btnPlceInCart, "Place in cart");
    		wait(15);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Add to cart is not visible" + e.getMessage());
            throw e;
        }
    }
    
    public void clickToCart(boolean flag) throws Exception {
    	try {
    		if(flag==true){
    			this.waitForElementToBePresent(this.chkSize, "Size");
        		this.click(this.chkSize, "Size");
        		this.waitForElementToBePresent(this.valueSize, "value");
        		this.click(this.valueSize, "value");   
        		this.waitForElementToBePresent(this.btnAddToCart, "Shopping Bag");
        		this.click(this.btnAddToCart, "Shopping Bag");
        		this.click(this.btnCart, "cart");
    		}else if(flag==false){
	    		this.waitForElementToBePresent(this.btnShoppingBag, "Shopping Bag");
	    		this.click(this.btnShoppingBag, "Shopping Bag");
	    		wait(5);
	    		this.waitForElementToBePresent(this.txtYourShoppingBag, "Your ShoppingBag");
	    		final boolean flagShoppingBag = Base.driver.findElement(this.txtYourShoppingBag).isDisplayed();
				assertTrue(flagShoppingBag);
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Your Shopping Bag is not visible" + e.getMessage());
            throw e;
        }
    }
    
    public void clickToCartForAll(boolean flag) throws Exception {
    	try {
    		if(flag==true){
    			wait(2);
    			this.waitForElementToBePresent(this.btnCart, "cart");
        		this.click(this.btnCart, "cart");
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Your Shopping Bag is not visible" + e.getMessage());
            throw e;
        }
    }
    
    public void clickToViewCartDetails() throws Exception {
    	try {
    		this.waitForElementToBePresent(this.btnViewCartDetails, "Viewcart Details");
    		this.click(this.btnViewCartDetails, "Viewcart Details");
    		this.waitForElementToBePresent(this.btnSubmit, "Submit");
    		final boolean flagSubmit = Base.driver.findElement(this.btnSubmit).isDisplayed();
			assertTrue(flagSubmit);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Submit is not visible" + e.getMessage());
            throw e;
        }
    }

      
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

    
}
