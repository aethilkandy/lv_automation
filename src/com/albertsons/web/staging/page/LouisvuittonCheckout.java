package com.albertsons.web.staging.page;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 06/11/2019 | page object class for louisvuitton checkout flow
*/

public class LouisvuittonCheckout extends ActionValidationLibrary {
	private static LouisvuittonCheckout louisvuittonCheckout;
	

	public static Logger logger = Logger.getLogger(LouisvuittonCheckout.class.getName());

	/******************************************************************************************************************************************
	 * Page objects for Albertsons Checkout Page Class
	 *****************************************************************************************************************************************/

	By txtCardNumber = By.xpath("//input[@id='creditCardNumber']");
	
	By txtCardHolder = By.xpath("//input[@id='creditCardHoldersName']");
	
	By cmbexpiryDate = By.xpath("//select[@id='expirationYear']");
	
	By valueExpDate = By.xpath("//option[@value='2022']");
	
	By txtSecurityCode = By.xpath("//div[@class='securityCode']//input[@type='tel']");
	
	By btnProceed = By.xpath("//button[@id='globalSubmit']");
	
	By btnSubmitOrder = By.xpath("//button[@id='globalSubmitTop']");
	
	By chkTermsOfPurchase = By.xpath("//input[@id='acceptTerms']");
	
	By txtCustomerName = By.xpath("//span[@class='thanksPageLabel']");
	
	

	private LouisvuittonCheckout() {

	}

	public static LouisvuittonCheckout getLouisvuittonCheckout() {

		if (louisvuittonCheckout.louisvuittonCheckout == null) {
			louisvuittonCheckout.louisvuittonCheckout = new LouisvuittonCheckout();
		}
		return louisvuittonCheckout.louisvuittonCheckout;
	}
	
	public void verifyCardDetails() throws Exception {
	  	try {
	  		DataLoader dataloader =new DataLoader();
	  		String credit_card_number =null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyCardDetails").get(0).get("credit_card_number")!=null){
    			credit_card_number  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyCardDetails").get(0).get("credit_card_number");
    			
    		}
    		driver.findElement(this.txtCardNumber).sendKeys(credit_card_number);
    		String credit_holder =null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyCardDetails").get(0).get("credit_holder")!=null){
    			credit_holder  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyCardDetails").get(0).get("credit_holder");
    			
    		}
    		driver.findElement(this.txtCardHolder).sendKeys(credit_holder);
    		this.click(this.cmbexpiryDate, "Expiry date");
    		this.click(this.valueExpDate, "exp date");
    		String security_Code =null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyCardDetails").get(0).get("security_Code")!=null){
    			security_Code  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyCardDetails").get(0).get("security_Code");
    		}
    		driver.findElement(this.txtSecurityCode).sendKeys(security_Code);
    		
    		final boolean flagProcedCheckout = Base.driver.findElement(this.btnProceed).isDisplayed();
			assertTrue(flagProcedCheckout);
			wait(3);
	  		}
	  	catch (final Exception e) {
	          ReporterListener.logFailureMessage("Failed ::to load 'Secure payment' option button" + e.getMessage());
	          throw e;
	      }
	  }
	
	public void clickOnProceedToCheckout() throws Exception {
    	try {
    		this.waitForElementToBePresent(btnProceed, "Proceed to checkout");
    		this.click(this.btnProceed, "Proceed to checkout");
    		wait(3);
    		this.waitForElementToBePresent(this.btnSubmitOrder, "Submit Order");
    		final boolean flagSubmitOrder = Base.driver.findElement(this.btnSubmitOrder).isDisplayed();
			assertTrue(flagSubmitOrder);
			wait(3);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to navigate to checkout page" + e.getMessage());
            throw e;
        }
    }
	
	public void verifySubmitOrder() throws Exception {
    	try {
    		scrollToBottom();
    		this.click(this.chkTermsOfPurchase, "Terms of purchase");
    		wait(3);
    		this.waitForElementToBePresent(this.btnSubmitOrder, "Submit Order");
    		this.click(this.btnSubmitOrder, "Submit Order");
    		final boolean flagCustomerName = Base.driver.findElement(this.txtCustomerName).isDisplayed();
			assertTrue(flagCustomerName);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to Order confirmation  page" + e.getMessage());
            throw e;
        }
    }
	
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

}
