package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for albertsons Homepage functionalities
*/
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetPageSource;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.javascript.host.geo.Coordinates;
import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
import com.steadystate.css.parser.Locatable;

public class AlbertsonsSandBoxes extends ActionValidationLibrary {
    private static AlbertsonsSandBoxes albertsonsSampleBoxPage;

    public static Logger logger = Logger.getLogger(AlbertsonsSandBoxes.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Home Page Class
     *****************************************************************************************************************************************/
    
    By headerTxt = By.xpath("//h2[text()='Explore Sample Box Collections ']");
    
    By cmbSort = By.xpath("//div[@id='amasty-shopby-product-list']//div[@class='toolbar toolbar-products'][1]//select[@class='sorter-options']");
    
    By sortValues = By.xpath("//option[@value='offer_min_price']");
    
    By collectioTitle = By.xpath("//div[@class='column main']");
    
   private AlbertsonsSandBoxes() {

    }
   
   public static AlbertsonsSandBoxes getAlbersonSampleBoxesPage() {

        if (AlbertsonsSandBoxes.albertsonsSampleBoxPage == null) {
            AlbertsonsSandBoxes.albertsonsSampleBoxPage = new AlbertsonsSandBoxes();
        }
        return AlbertsonsSandBoxes.albertsonsSampleBoxPage;
    }
    
   public void verifyHeaderText() throws Exception {
   	try {
   		scrollTo(this.headerTxt, "Header text");
   		this.waitForElementToBeVisible(this.headerTxt, "Header text");
   		final boolean flagtHeaderTxt = Base.driver.findElement(this.headerTxt).isDisplayed();
	        assertTrue(flagtHeaderTxt);
   		}
   	catch (final Exception e) {
           ReporterListener.logFailureMessage("Failed ::Header text is not visible" + e.getMessage());
           throw e;
       }
   }
   
   public void sortOptionVerification() throws Exception {
	   	try {
	   		JavascriptExecutor js = (JavascriptExecutor) driver;
	   		js.executeScript("javascript:window.scrollBy(0,50)");
	   		this.waitForElementToBeVisible(this.cmbSort, "Sort option");
	   		this.click(this.cmbSort, "Sort option");
	   		wait(3);
	   		this.click(this.sortValues, "Sort values");
	   		}
	   	catch (final Exception e) {
	           ReporterListener.logFailureMessage("Failed ::Header text is not visible" + e.getMessage());
	           throw e;
	       }
	   }
    
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

    
}
