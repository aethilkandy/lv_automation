package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for albertsons Homepage functionalities
*/
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetPageSource;
import org.testng.annotations.Test;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;

public class AlbertsonsHomeFooter extends ActionValidationLibrary {
    private static AlbertsonsHomeFooter albertsonsHomeFooterPage;

    public static Logger logger = Logger.getLogger(AlbertsonsHomeFooter.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Home Page Class
     *****************************************************************************************************************************************/
    
    By textRightReserved = By.xpath("//span[text()='� 2018 Infinite Aisle LLC companies, LLC. All rights reserved']");
    
    By lnkAboutMarketPlace = By.xpath("//div[@class='std']/div[1]//a[text()='About Marketplace']");
    
    By lnkHowMarketplaceWorks = By.xpath("//div[@class='std']/div[1]//a[contains(text(),'How Marketplace works')]");
    
    By lnkContactMarketplace = By.xpath("//div[@class='std']/div[1]//a[contains(text(),'Contact Marketplace')]");
    
    By lnkFAQ = By.xpath("//div[@class='std']/div[1]//a[contains(text(),'FAQ')]");
    
    By lnkSellerStories = By.xpath("//div[@class='std']/div[2]//a[contains(text(),'Seller Stories')]");
    
    By lnkBecomeSeller = By.xpath("//div[@class='std']/div[2]//a[contains(text(),'Become a Seller')]");
    
    By lnkProductRequest = By.xpath("//div[@class='std']/div[2]//a[contains(text(),'Product Requests')]");
    
    By lnkAdchoice = By.xpath("//div[@class='std']/div[2]//a[contains(text(),'AdChoices')]");
    
    By lnkTermsOfUse = By.xpath("//div[@class='std']/div[3]//a[contains(text(),'Terms of Use')]");
    
    By lnkPrivacyPloicy = By.xpath("//div[@class='std']/div[3]//a[contains(text(),'Privacy Policy')]");
    
    By lnkAccessibilityPolicy = By.xpath("//div[@class='std']/div[3]//a[contains(text(),'Accessibility Policy')]");
    
    By txtAboutMarketPlace = By.xpath("//h1[text()='About Marketplace']");
    
    By txtHowMarketPlaceWorks = By.xpath("//a[@aria-label='How Marketplace works']");
    
    By txtContactMarketPlace = By.xpath("//a[@aria-label='Contact Marketplace']");
    
    By txtFAQs = By.xpath("//strong[text()='FAQs']");
    
    By txtSellerStory = By.xpath("//a[contains(text(),'Seller Stories ')]");
    
    By txtBecomeSeller = By.xpath("//strong[text()='Become a Seller on Marketplace']");
    
    By txtProductRequest = By.xpath("//h1[contains(text(),' PRODUCT REQUESTS')]");
    
    By txtAdChoice = By.xpath("//strong[text()='Ad Choices']");
    
    By txtTermsOfUse = By.xpath("//strong[contains(text(),'Terms of Use')]");
    
    By txtPrivacyPolicy = By.xpath("//strong[contains(text(),'Privacy Policy')]");
    
    By txtAccessibilityPolicy = By.xpath("//strong[contains(text(),'ACCESSIBILITY POLICY')]");
    
    By txtDataScore = By.xpath("//button[@data-score='10']");
    
    By txtComment = By.xpath("//textarea[@name='comment']");
    
    By btnSubmit = By.xpath("//button[@name='button']");

    private AlbertsonsHomeFooter() {

    }
   
   public static AlbertsonsHomeFooter getAlbertsonsHomeFooterPage() {

        if (AlbertsonsHomeFooter.albertsonsHomeFooterPage == null) {
            AlbertsonsHomeFooter.albertsonsHomeFooterPage = new AlbertsonsHomeFooter();
        }
        return AlbertsonsHomeFooter.albertsonsHomeFooterPage;
    }
    
   
	
    public void rightReservedTextVerification() throws Exception {
    	try {
			this.scrollToBottom();
			this.waitForElementToBeVisible(this.textRightReserved, "Rights reserved text");
	        final boolean flagtextRightReserved = Base.driver.findElement(this.textRightReserved).isDisplayed();
	        assertTrue(flagtextRightReserved);			
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void aboutMarketplaceLinkVerification() throws Exception {
    	try {
    		scrollToBottom();
			this.waitForElementToBeVisible(this.lnkAboutMarketPlace, "About Marketplace");
	        final boolean flagtextAboutMarketPlace = Base.driver.findElement(this.lnkAboutMarketPlace).isDisplayed();
	        assertTrue(flagtextAboutMarketPlace);	
	        this.click(this.lnkAboutMarketPlace, "About Marketplace");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String urlAboutmarketplace = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","aboutMarketplaceLinkVerification").get(0).get("ABOUT_URL")!=null){
    			urlAboutmarketplace =(String)dataloader.getTestScenarios("MOREFORYOU.json","aboutMarketplaceLinkVerification").get(0).get("ABOUT_URL");
    			 Base.driver.get(urlAboutmarketplace);
		         waitForElementToBePresent(this.txtAboutMarketPlace, "About Marketplace");
		         final boolean flag = Base.driver.findElement(this.txtAboutMarketPlace).isDisplayed();
		         assertTrue(flag);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void howMarketplaceWorksLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkHowMarketplaceWorks, "How Marketplace works");
	        final boolean flagtextHowMarketPlaceWorks = Base.driver.findElement(this.lnkHowMarketplaceWorks).isDisplayed();
	        assertTrue(flagtextHowMarketPlaceWorks);	
	        scrollTo(this.lnkHowMarketplaceWorks, "How MarketplaceWorks");
	        this.click(this.lnkHowMarketplaceWorks, "How MarketplaceWorks");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String howMarketplaceWorks = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","howMarketplaceWorksLinkVerification").get(0).get("WORKS_URL")!=null){
    			howMarketplaceWorks =(String)dataloader.getTestScenarios("MOREFORYOU.json","howMarketplaceWorksLinkVerification").get(0).get("WORKS_URL");
    			 Base.driver.get(howMarketplaceWorks);
		         waitForElementToBePresent(this.txtHowMarketPlaceWorks, "How Marketplace works");
		         final boolean flag = Base.driver.findElement(this.txtHowMarketPlaceWorks).isDisplayed();
		         assertTrue(flag);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void contactMarketplaceLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkContactMarketplace, "Contact marketplace");
	        final boolean flagtextContactMarketPlace = Base.driver.findElement(this.lnkContactMarketplace).isDisplayed();
	        assertTrue(flagtextContactMarketPlace);	
	        scrollTo(this.lnkContactMarketplace, "contact marketplace");
	        this.click(this.lnkContactMarketplace, "contact marketplace");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String contactMarketplace = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","contactMarketplaceLinkVerification").get(0).get("CONTACT_URL")!=null){
    			contactMarketplace =(String)dataloader.getTestScenarios("MOREFORYOU.json","contactMarketplaceLinkVerification").get(0).get("CONTACT_URL");
    			 Base.driver.get(contactMarketplace);
		         waitForElementToBePresent(this.txtContactMarketPlace, "contact Marketplace");
		         final boolean flag = Base.driver.findElement(this.txtContactMarketPlace).isDisplayed();
		         assertTrue(flag);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void FAQLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkFAQ, "FAQ");
	        final boolean flagtextFAQ = Base.driver.findElement(this.lnkFAQ).isDisplayed();
	        assertTrue(flagtextFAQ);	
	        scrollTo(this.lnkFAQ, "FAQ");
	        this.click(this.lnkFAQ, "FAQ");
//	        ();
	       }
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void delightMessageLinkVerification() throws Exception {
    	try{
    		this.waitForElementToBeVisible(this.txtDataScore, "Data score");
			assertTrue(isElementDisplayed(this.txtDataScore, "Data Score"));
			this.click(this.txtDataScore, "Data score");
			driver.findElement(this.txtComment).sendKeys("highly recommended");
			this.waitForElementToBeClickable(this.btnSubmit, "Submit");
			assertTrue(isElementDisplayed(this.btnSubmit, "Submit"));
			this.click(this.btnSubmit, "Submit");
			 wait(3);
		        DataLoader dataloader =new DataLoader();
		        String FAQ = null;
	    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","FAQLinkVerification").get(0).get("URL")!=null){
	    			FAQ =(String)dataloader.getTestScenarios("MOREFORYOU.json","FAQLinkVerification").get(0).get("URL");
	    			 Base.driver.get(FAQ);
			         waitForElementToBePresent(this.txtFAQs, "FAQ");
			         final boolean flag = Base.driver.findElement(this.txtFAQs).isDisplayed();
			         assertTrue(flag);
			         wait(3);
		        driver.navigate().back();
	    		}

    	}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void sellerStoriesLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkSellerStories, "Seller stories");
	        final boolean flagtextSellerStories = Base.driver.findElement(this.lnkSellerStories).isDisplayed();
	        assertTrue(flagtextSellerStories);	
	        scrollTo(this.lnkSellerStories, "Seller stories");
	        this.click(this.lnkSellerStories, "Seller stories");
	        wait(4);
	        DataLoader dataloader =new DataLoader();
	        String sellerStories = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","sellerStoriesLinkVerification").get(0).get("SELLERSTORY_URL")!=null){
    			sellerStories =(String)dataloader.getTestScenarios("MOREFORYOU.json","sellerStoriesLinkVerification").get(0).get("SELLERSTORY_URL");
    			 Base.driver.get(sellerStories);
    			 String url = driver.getCurrentUrl();
//		         waitForElementToBePresent(this.txtSellerStory, "Seller Story");
//		         final boolean flag = Base.driver.findElement(this.txtSellerStory).isDisplayed();
		         assertEquals(url,"https://staging-marketplace.albertsons.com/story/canvas");
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void becomeSellerLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkBecomeSeller, "Become a seller");
	        final boolean flagtextBecomeSeller = Base.driver.findElement(this.lnkBecomeSeller).isDisplayed();
	        assertTrue(flagtextBecomeSeller);	
	        scrollTo(this.lnkBecomeSeller, "Become a seller");
	        this.click(this.lnkBecomeSeller, "Become a seller");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String sellerStories = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","becomeSellerLinkVerification").get(0).get("BECOMESELLER_URL")!=null){
    			sellerStories =(String)dataloader.getTestScenarios("MOREFORYOU.json","becomeSellerLinkVerification").get(0).get("BECOMESELLER_URL");
    			 Base.driver.get(sellerStories);
    			 String url = driver.getCurrentUrl();
//		         waitForElementToBePresent(this.txtBecomeSeller, "Become a seller");
//		         final boolean flag = Base.driver.findElement(this.txtBecomeSeller).isDisplayed();
		         assertEquals(url,"https://staging-marketplace.albertsons.com/become-a-seller-on-marketplace");
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void productRequestLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkProductRequest, "Product Request");
	        final boolean flagtextProductRequest = Base.driver.findElement(this.lnkProductRequest).isDisplayed();
	        assertTrue(flagtextProductRequest);	
	        scrollTo(this.lnkProductRequest, "Product Request");
	        this.click(this.lnkProductRequest, "Product Request");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String productRequest = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","productRequestLinkVerification").get(0).get("PRODUCTREQ_URL")!=null){
    			productRequest =(String)dataloader.getTestScenarios("MOREFORYOU.json","productRequestLinkVerification").get(0).get("PRODUCTREQ_URL");
    			 Base.driver.get(productRequest);
    			 wait(10);
		         waitForElementToBePresent(this.txtProductRequest, "Product requests");
		         final boolean flag = Base.driver.findElement(this.txtProductRequest).isDisplayed();
		         assertTrue(flag);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void adchoiceLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkAdchoice, "Ad choice");
	        final boolean flagtextAdchoice = Base.driver.findElement(this.lnkAdchoice).isDisplayed();
	        assertTrue(flagtextAdchoice);
	        scrollTo(this.lnkAdchoice, "Ad choice");
	        this.click(this.lnkAdchoice, "Ad choice");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String adChoice = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","adchoiceLinkVerification").get(0).get("URL")!=null){
    			adChoice =(String)dataloader.getTestScenarios("MOREFORYOU.json","adchoiceLinkVerification").get(0).get("URL");
    			 Base.driver.get(adChoice);
		         waitForElementToBePresent(this.txtAdChoice, "AdChoice");
		         final boolean flag = Base.driver.findElement(this.txtAdChoice).isDisplayed();
		         assertTrue(flag);
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void termsOfUseLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkTermsOfUse, "Terms of use");
	        final boolean flagtextTermsOfUse = Base.driver.findElement(this.lnkTermsOfUse).isDisplayed();
	        assertTrue(flagtextTermsOfUse);	
	        scrollTo(this.lnkTermsOfUse, "Terms of use");
	        this.click(this.lnkTermsOfUse, "Terms of use");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String termsOfUse = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","termsOfUseLinkVerification").get(0).get("TERMS_URL")!=null){
    			termsOfUse =(String)dataloader.getTestScenarios("MOREFORYOU.json","termsOfUseLinkVerification").get(0).get("TERMS_URL");
    			 Base.driver.get(termsOfUse);
    			 String url = driver.getCurrentUrl();
//		         waitForElementToBePresent(this.txtTermsOfUse, "Terms of use");
//		         final boolean flag = Base.driver.findElement(this.txtTermsOfUse).isDisplayed();
		         assertEquals(url,"https://staging-marketplace.albertsons.com/terms-of-use");
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void privacyPolicyLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkPrivacyPloicy, "privacy policy");
	        final boolean flagtextPrivacyPolicy = Base.driver.findElement(this.lnkPrivacyPloicy).isDisplayed();
	        assertTrue(flagtextPrivacyPolicy);	
	        scrollTo(this.lnkPrivacyPloicy, "Privacy Policy");
	        this.click(this.lnkPrivacyPloicy, "Privacy Policy");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String privacyPolicy = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","privacyPolicyLinkVerification").get(0).get("PRIVACY_URL")!=null){
    			privacyPolicy =(String)dataloader.getTestScenarios("MOREFORYOU.json","privacyPolicyLinkVerification").get(0).get("PRIVACY_URL");
    			 Base.driver.get(privacyPolicy);
    			 String url = driver.getCurrentUrl();
//		         waitForElementToBePresent(this.txtPrivacyPolicy, "Privacy Policy");
//		         final boolean flag = Base.driver.findElement(this.txtPrivacyPolicy).isDisplayed();
		         assertEquals(url,"https://staging-marketplace.albertsons.com/privacy-policy");
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
    public void accessibilityLinkVerification() throws Exception {
    	try {
			this.waitForElementToBeVisible(this.lnkAccessibilityPolicy, "Accessibility policy");
	        final boolean flagaccessibilityPolicy = Base.driver.findElement(this.lnkAccessibilityPolicy).isDisplayed();
	        assertTrue(flagaccessibilityPolicy);	
	        scrollTo(this.lnkAccessibilityPolicy, "Accessibility Policy");
	        this.click(this.lnkAccessibilityPolicy, "Accessibility Policy");
	        wait(3);
	        DataLoader dataloader =new DataLoader();
	        String accessibilityPolicy = null;
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","accessibilityLinkVerification").get(0).get("ACCESSIBILITY_URL")!=null){
    			accessibilityPolicy =(String)dataloader.getTestScenarios("MOREFORYOU.json","accessibilityLinkVerification").get(0).get("ACCESSIBILITY_URL");
    			 Base.driver.get(accessibilityPolicy);
    			 String url = driver.getCurrentUrl();
//		         waitForElementToBePresent(this.txtAccessibilityPolicy, "Accessibility Policy");
//		         final boolean flag = Base.driver.findElement(this.txtAccessibilityPolicy).isDisplayed();
		         assertEquals(url,"https://staging-marketplace.albertsons.com/accessibility-policy");
	        driver.navigate().back();
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::No of columns in the footer are incorrect" + e.getMessage());
            throw e;
        }
    }
    
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

    
}
