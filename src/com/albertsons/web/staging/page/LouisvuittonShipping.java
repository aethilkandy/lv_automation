package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import javax.xml.xpath.XPath;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetElementText;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 06/07/2019 | page object class for Louisvuitton shipping functionality
*/

public class LouisvuittonShipping extends ActionValidationLibrary {
	private static LouisvuittonShipping louisvuittonShipping;
	
	public static Logger logger = Logger.getLogger(LouisvuittonShipping.class.getName());

	/******************************************************************************************************************************************
	 * Page objects for Louisvuitton shipping Page Class
	 *****************************************************************************************************************************************/

	By firstName = By.xpath("//input[@id='firstName']");
	
	By lastName = By.xpath("//input[@id='lastName']");
	
	By country  = By.xpath("//select[@id='country']");
	
	By addressLineOne =By.xpath("//input[@id='address1']");
	
	By addressLineTwo =By.xpath("//input[@id='address2']");
	
	By city =By.xpath("//input[@id='city']");
	
	By txtEmail = By.xpath("//input[@id='email']");
	
	By postCode =By.xpath("//input[@id='postalCode']");
	
	By telephone =By.xpath("//input[@id='phoneNumber']");
	
	By proceedBilling = By.xpath("//input[@id='globalSubmit']");
	
	By prcdBilling = By.xpath("//button[@id='globalSubmit']");
	
	By btnPrdBilling = By.xpath("//button[@id='globalSubmit']");
	
	By txtSecurePayment = By.xpath("//button[@id='securePayment']");
	
	By cmbPhoneNumberType = By.xpath("//select[@id='phoneNumberType']");
	
	By numberTypeValue = By.xpath("//select[@id='phoneNumberType']//option[@value='home']");
	
	By cmbCountry = By.xpath("//select[@id='country']");
	
	By countryValue = By.xpath("//select[@id='country']//option[@value='SE']");
	
	By toglePremiumDelivery = By.xpath("//input[@id='deliverypremiumSE']");
	
	By txtShipping = By.xpath("//main[@class='content']//div[@class='orderSummary onlyAS']//div[@id='tableProducts']//ul[@id='tableTotal']//li[@id='shippingBlock']//span[@class='shippingText']");
	
	By txtSubTotalValue = By.xpath("//main[@class='content']//div[@class='orderSummary onlyAS']//div[@id='tableProducts']//ul[@id='tableTotal']//li//div[@id='subtotalBlock']//span[@class='subtotalText']");
	
	By cmbPhoneCountryId = By.xpath("//select[@id='phoneNumberCountry']");
	
	By valuePhnCountry = By.xpath("//select[@id='phoneNumberCountry']//option[@value='NL']");
	
	By chkBanContact = By.xpath("//input[@id='bancontactSelect']");

	private LouisvuittonShipping() {

	}

	public static LouisvuittonShipping getLouisvuittonShipping() {

		if (LouisvuittonShipping.louisvuittonShipping == null) {
			LouisvuittonShipping.louisvuittonShipping = new LouisvuittonShipping();
		}
		return LouisvuittonShipping.louisvuittonShipping;
	}
	
  public void verifyBillingAddress(boolean flag) throws Exception {
		try {
			this.waitForElementToBeVisible(this.firstName, "First name");
			String firstName=null;
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("First_Name")!=null){
    			firstName  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("First_Name");
    			
    		}
    		driver.findElement(this.firstName).sendKeys(firstName);
    		String lastName=null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Last_Name")!=null){
    			lastName  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Last_Name");
    			
    		}
    		driver.findElement(this.lastName).sendKeys(lastName);
    		String addressLineOne =null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Address_One")!=null){
    			addressLineOne  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Address_One");
    			
    		}
    		driver.findElement(this.addressLineOne).sendKeys(addressLineOne);
    		String addressLineTwo =null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Address_Two")!=null){
    			addressLineTwo  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Address_Two");
    			
    		}
    		driver.findElement(this.addressLineTwo).sendKeys(addressLineTwo);
    		String Postal_Code =null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Postal_Code")!=null){
    			Postal_Code  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Postal_Code");
    			
    		}
    		driver.findElement(this.postCode).sendKeys(Postal_Code);
    		String City =null;
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("City")!=null){
    			City  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("City");
    			
    		}
    		driver.findElement(this.city).sendKeys(City);
    		
    		this.click(this.cmbPhoneNumberType,"Phone number type");
      		this.click(this.numberTypeValue, "Number Type");
    		
    		String Phone_Number =null;    		
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Phone_Number")!=null){
    			Phone_Number  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Phone_Number");
    			
    		}
    		driver.findElement(this.telephone).sendKeys(Phone_Number);
    		
    		String Email_Id =null;    		
    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Email")!=null){
    			Email_Id  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Email");
    			
    		}
    		if(flag==true){
    		driver.findElement(this.txtEmail).sendKeys(Email_Id);
    		final boolean flagProceddBilling = Base.driver.findElement(this.proceedBilling).isDisplayed();
			assertTrue(flagProceddBilling);
    		}
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to click checkout button" + e.getMessage());
	        throw e;
	    }
	}
  
  public void verifyChangeBillingAddress(boolean flag) throws Exception {
		try {
			this.waitForElementToBeVisible(this.firstName, "First name");
			String firstName=null;
			DataLoader dataloader =new DataLoader();
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("First_Name")!=null){
	  			firstName  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("First_Name");
	  			
	  		}
	  		driver.findElement(this.firstName).sendKeys(firstName);
	  		String lastName=null;
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Last_Name")!=null){
	  			lastName  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Last_Name");
	  			
	  		}
	  		driver.findElement(this.lastName).sendKeys(lastName);
	  		String addressLineOne =null;
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Address_One")!=null){
	  			addressLineOne  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Address_One");
	  			
	  		}
	  		this.click(this.cmbCountry, "Contry name");
	  		this.click(this.countryValue, "Country Value");
	  		
	  		driver.findElement(this.addressLineOne).sendKeys(addressLineOne);
	  		String addressLineTwo =null;
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Address_Two")!=null){
	  			addressLineTwo  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Address_Two");
	  			
	  		}
	  		driver.findElement(this.addressLineTwo).sendKeys(addressLineTwo);
	  		String Postal_Code =null;
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Postal_Code")!=null){
	  			Postal_Code  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Postal_Code");
	  			
	  		}
	  		driver.findElement(this.postCode).sendKeys(Postal_Code);
	  		String City =null;
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("City")!=null){
	  			City  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("City");
	  			
	  		}
	  		driver.findElement(this.city).sendKeys(City);
  		
	  		this.click(this.cmbPhoneNumberType,"Phone number type");
    		this.click(this.numberTypeValue, "Number Type");
    		this.click(this.cmbPhoneCountryId,"Phone number Id");
    		this.click(this.valuePhnCountry, "Phone number Country value");
  		
	  		String Phone_Number =null;    		
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Phone_Number")!=null){
	  			Phone_Number  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Phone_Number");
	  			
	  		}
	  		driver.findElement(this.telephone).sendKeys(Phone_Number);
  		
	  		String Email_Id =null;    		
	  		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Email")!=null){
	  			Email_Id  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyChangeBillingAddress").get(0).get("Email");
	  			
	  		}
	  		if(flag==true){
	  		driver.findElement(this.txtEmail).sendKeys(Email_Id);
	  		final boolean flagProceddBilling = Base.driver.findElement(this.proceedBilling).isDisplayed();
				assertTrue(flagProceddBilling);
	  		}
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to click checkout button" + e.getMessage());
	        throw e;
	    }
	}
  
  public void clickOnBanContact() throws Exception {
	  	try {
		  		this.waitForElementToBePresent(this.chkBanContact, "BanContact");
		  		this.click(this.chkBanContact, "BanContact");
		  		
		  		final boolean flagProceedBilling = Base.driver.findElement(this.prcdBilling).isDisplayed();
				assertTrue(flagProceedBilling);
				wait(3);
				
				this.waitForElementToBePresent(this.prcdBilling, "Proceed Billing");
		  		this.click(this.prcdBilling, "Proceed Billing");
	  		}
	  	catch (final Exception e) {
	          ReporterListener.logFailureMessage("Failed ::to load 'flagProceedBilling' option button" + e.getMessage());
	          throw e;
	      }
	  }
  
  public void verifyProceedToBilling(boolean flag) throws Exception {
	  	try {
	  		if(flag==true)
	  		{
	  		this.waitForElementToBePresent(this.proceedBilling, "Proceed Billing");
	  		this.click(this.proceedBilling, "Proceed Billing");
	  		}else{
	  			this.waitForElementToBePresent(this.btnPrdBilling, "Proceed Billing");
	  	  		this.click(this.btnPrdBilling, "Proceed Billing");
	  		}
	  		
	  		this.waitForElementToBePresent(this.txtSecurePayment, "Secure Payment");
	  		final boolean flagSecurePayment = Base.driver.findElement(this.txtSecurePayment).isDisplayed();
				assertTrue(flagSecurePayment);
				wait(3);
	  		}
	  	catch (final Exception e) {
	          ReporterListener.logFailureMessage("Failed ::to load 'Secure payment' option button" + e.getMessage());
	          throw e;
	      }
	  }
	
  public void selectPremiemDelivery() throws Exception {
  	try {
  		this.waitForElementToBePresent(this.toglePremiumDelivery, "Premium Delivery");
  		this.click(this.toglePremiumDelivery, "Premium Delivery");
  		this.waitForElementToBePresent(this.txtShipping, "text shipping");
  		wait(5);
  		}
  	catch (final Exception e) {
          ReporterListener.logFailureMessage("Failed ::to load 'Secure payment' option button" + e.getMessage());
          throw e;
      }
  }
  
  
  public void verifyBillingAddressForRegisteredUser() throws Exception {
	  	try {
	  		this.waitForElementToBePresent(this.proceedBilling, "Proceed Billing");
	  		this.click(this.proceedBilling, "Proceed Billing");
//	  		this.waitForElementToBePresent(this.txtShipping, "text shipping");
	  		wait(2);
	  		}
	  	catch (final Exception e) {
	          ReporterListener.logFailureMessage("Failed ::to load 'Secure payment' option button" + e.getMessage());
	          throw e;
	      }
	  }
  
  public void currencyVerification() throws Exception {
	  	try {
	  //		scrollToMid();
	  		String value = driver.findElement(this.txtSubTotalValue).getTagName();
	  		System.out.println("valuevaluevalue---"+driver.findElement(this.txtSubTotalValue)+"valuevalue"+value);
	  		this.waitForElementToBePresent(this.toglePremiumDelivery, "Premium Delivery");
	  		this.click(this.toglePremiumDelivery, "Premium Delivery");
	  		this.waitForElementToBePresent(this.txtShipping, "text shipping");
	  		}
	  	catch (final Exception e) {
	          ReporterListener.logFailureMessage("Failed ::to load 'Secure payment' option button" + e.getMessage());
	          throw e;
	      }
	  }
  
  
		
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

}
