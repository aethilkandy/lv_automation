package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 06/13/2019 | page object class for Louisvuitton signup functionality
*/
public class LouisvuittonSignUp extends ActionValidationLibrary {
    private static LouisvuittonSignUp louisvuittonSignUp;

    public static Logger logger = Logger.getLogger(LouisvuittonSignUp.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Louisvuitton SignUp Class
     *****************************************************************************************************************************************/
    By btnSignUp =By.xpath("//div[@class='actions button-block']");
    
    By txtAcntcreation =By.xpath("//span[text()='Account creation']");
    
    By firstName =By.xpath("//input[@name='firstname']");
    
    By lastName =By.xpath("//input[@name='lastname']");
    
    By phoneNumber =By.xpath("//input[@name='phone_number']");
    
    By postCode =By.xpath("//input[@name='postcode']");
    
    By isSuscribed =By.xpath("//input[@name='is_subscribed']");
    
    By email =By.xpath("//input[@name='email']");
    
    
   private LouisvuittonSignUp() {

    }

    public static LouisvuittonSignUp getLouisvuittonSignUp() {

        if (LouisvuittonSignUp.louisvuittonSignUp == null) {
            LouisvuittonSignUp.louisvuittonSignUp = new LouisvuittonSignUp();
        }
        return LouisvuittonSignUp.louisvuittonSignUp;
    }
   
    public void verifyAlbertsonsSignUpButton() throws Exception {
    	try {
    		 this.waitForElementToBeVisible(this.btnSignUp, "Sign Up");
    		 this.click(this.btnSignUp, "Sign Up");
    		 assertTrue(isElementDisplayed(txtAcntcreation, "Account creation"));
    		 wait();
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to see SignUp page" + e.getMessage());
            throw e;
        }
    }  	
    
      
    private void turnOffImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    
}
