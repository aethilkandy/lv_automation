package com.albertsons.web.staging.page;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.louisvuitton.test.BaseDriver;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 08/08/2018 | page object class for albertsons Myaccount functionality
*/
public class AlbertsonsMyAccount extends ActionValidationLibrary {
    private static AlbertsonsMyAccount albertsonsMyAccount;

    public static Logger logger = Logger.getLogger(AlbertsonsMyAccount.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons My Account Class
     *****************************************************************************************************************************************/
    By myAccount =By.xpath("//img[@alt='My Account']");
    
    By labelMyAccount =By.xpath("//a[text()='Account']");
    
    By signUpText = By.xpath("//span[contains(text(),'Welcome, ')]");
    
    By postCode =By.xpath("//input[@name='postcode']");
    
    By isSuscribed =By.xpath("//input[@name='is_subscribed']");
    
    By email =By.xpath("//input[@name='email']");
    
    By welcomeText =By.xpath("//select[@name='sec_question']");
    
    By selectQuestion =By.xpath("//input[@name='password_confirmation']");
    
    By yourAnswer =By.xpath("//input[@name='answer']");
    
    By signUpButton =By.xpath("//input[@value='Sign In']");
    
    By mainTitle =By.xpath("//h1[@class='main-title']");
    
    By shopTitle =By.xpath("//div[@class='section shopTiles']");
    
    By shopHealthAndBeauty =By.xpath("//div//p[text()='HEALTH AND BEAUTY']");
    
    By shopTitleHouseHold =By.xpath("//div//p[text()='HOUSE HOLD'])");
    
    By shopTitleGrocery =By.xpath("//div//p[text()='GROCERY'])");
    
    By noOfProductInfo =By.xpath("//div[@class='owl-wrapper']//div[@class='owl-item']//div//div[@class='product-item-info'])");
    
    By textRegisteredCustomers = By.xpath("//strong[text()='Registered Customers']");
    
    By textAccountInformation = By.xpath("//div[@class='block-title']//strong[text()='Account Information']");
    
    By textMyDashboard  = By.xpath("//div[@class='page-title']//h1[text()=' Account']");
    
    By textAccountDashboard = By.xpath("//a[text()='Account Information']");
    
    By textAddressBook  = By.xpath("//strong[text()='Address Book']");
    
    By textAccountInformationTab = By.xpath("//a[text()='Account Information']");
    
    By textNewsletterSubscription = By.xpath("//li[@class='nav item']//a[text()='Newsletter Subscription']");
    
    By textNewsletterSubscriptionSave =By.xpath("//span[text()='Save']");
    
    By textFirstAndLastName = By.xpath("//div[@class='box-content']//p[contains(text(),'aamy pattali')]");
    
    By editButtonInContactInfo = By.xpath("//div[@class='box box-information']//div[@class='box-actions']//a//span[text()='Edit']");
    
    By passwordButtonInContactInfo =By.xpath("//div[@class='box box-information']//div[@class='box-actions']//a//span[text()='Change Password']");
    
    By textSubscriptionStatus = By.xpath("//span[text()='Newsletters']/parent::strong/parent::div//div//p");
    
    By editSubscriptionStatus = By.xpath("//span[text()='Newsletters']/parent::strong/parent::div/div[@class='box-actions']//span");
    
    By manageAddress = By.xpath("//span[text()='Account Information']");
    
    By contactInformation = By.xpath("//span[text()='Contact Information']");
    
    By defaultBillingAddress = By.xpath("//span[text()='Default Billing Address']");
    
    By defaultShippingAddress = By.xpath("//span[text()='Default Shipping Address']");
    
    By editShippingAddress = By.xpath("//div[@class='box box-shipping-address']/child::div[@class='box-actions']//a");
    
    By editBillingAddress = By.xpath("//div[@class='box box-billing-address']/child::div[@class='box-actions']//a");
    
    By textInvalidUsername = By.xpath("//div[@id='okta-signin-username-error']");
    
    By textFirstName = By.xpath("//input[@id='firstname']");
    
    By textLastName = By.xpath("//input[@id='lastname']");
    
    By textPhone = By.xpath("//input[@id='phone_number']");
    
    By chkboxSubscription = By.xpath("//input[@id='subscription']");
    
    By textViewAll = By.xpath("//span[text()='View All']");
    
    By txtRecentOrders = By.xpath("//strong[text()='Recent Orders']");
    
    By txtMyOrders = By.xpath("//a[text()='My Orders']");
    
    By textMyAccount  = By.xpath("//div[@class='page-title']//h1[text()=' Account']");
    
    By txtNoOrder = By.xpath("//span[text()='There are currently no orders to display.']");
    
    By lnkEditAddress = By.xpath("//span[text()='Edit Address']/parent::a[@aria-label='Edit Default Billing Address']");
    
    By txtContactInformation = By.xpath("//span[text()='Contact Information']");
    
    By txtPasswordChangingInfo = By.xpath("//p[@id='password-change-infomsg']");
    
    By lnkChngePwd = By.xpath("//span[text()='Change Password']");
    
    By txtAddressBook = By.xpath("//strong[text()='Address Book']");
    
    By txtNewsLetter = By.xpath("//span[text()='Newsletters']");
    
    By lnkEdit = By.xpath("//span[text()='Change Password']/ancestor::div[@class='box-actions']//a//span[text()='Edit']");
    
   AlbertsonsMyAccount() {

    }

    public static AlbertsonsMyAccount getAlbersonsMyAccount() {

        if (AlbertsonsMyAccount.albertsonsMyAccount == null) {
            AlbertsonsMyAccount.albertsonsMyAccount = new AlbertsonsMyAccount();
        }
        return AlbertsonsMyAccount.albertsonsMyAccount;
    }
    
    public void verifyMyDashboard() throws Exception{
    	try{
    		this.waitForElementToBePresent(this.textAccountInformation, "Account Information");
	    	assertTrue(isElementDisplayed(this.textAccountInformation, "Account Information"));
	    	this.waitForElementToBePresent(this.textMyAccount, "My Account");
	    	assertTrue(isElementDisplayed(this.textMyAccount, "My Account"));
	    	scrollTo(this.txtContactInformation, "Contact Information");
	    	
	    	this.waitForElementToBePresent(this.txtContactInformation, "Contact Information");
	    	assertTrue(isElementDisplayed(this.txtContactInformation, "Contact Information"));
//	    	this.waitForElementToBePresent(this.txtName, "Name");
//	    	assertTrue(isElementDisplayed(this.txtName, "Name"));
//	    	this.waitForElementToBePresent(this.txtMail, "Mail");
//	    	assertTrue(isElementDisplayed(this.txtMail, "Mail"));
	    	this.waitForElementToBePresent(this.txtPasswordChangingInfo, "Password Changing info");
	    	assertTrue(isElementDisplayed(this.txtPasswordChangingInfo, "Password Changing info"));
	    	this.waitForElementToBePresent(this.lnkChngePwd, "Change password");
	    	assertTrue(isElementDisplayed(this.lnkChngePwd, "Change password"));
	    	this.waitForElementToBePresent(this.lnkEdit, "Edit");
	    	assertTrue(isElementDisplayed(this.lnkEdit, "Edit"));
	    	this.waitForElementToBePresent(this.txtNewsLetter, "News letter");
	    	assertTrue(isElementDisplayed(this.txtNewsLetter, "News letter"));
	    	this.waitForElementToBePresent(this.txtAddressBook, "Address Book");
	    	assertTrue(isElementDisplayed(this.txtAddressBook, "Address Book"));
	    	this.waitForElementToBePresent(this.txtAddressBook, "Address Book");
	    	assertTrue(isElementDisplayed(this.txtAddressBook, "Address Book"));
    		
	    	}catch(final Exception e){
	    		ReporterListener.logFailureMessage("Failed ::to get the First and Lastname of the user" + e.getMessage());
		        throw e;
	    	}
    }
   

//	public void navigateToMyAccount() throws Exception {
//		try {
//			
//			this.waitForElementToBeVisible(this.myAccount, "My Account");
//			this.click(this.myAccount, "My Account");
//			this.waitForElementToBeVisible(this.labelMyAccount, "Label My Account");
//			this.click(this.labelMyAccount, "Label My Account");
//			String URLMyAccount = driver.getCurrentUrl();
//	    	Assert.assertEquals(URLMyAccount, "https://staging-marketplace.albertsons.com/customer/account/" );
//			}
//		catch (final Exception e) {
//	        ReporterListener.logFailureMessage("Failed ::to get the First and Lastname of the user" + e.getMessage());
//	        throw e;
//	    }
//	}

	public void getVerifyAccountTabDetails() throws Exception {
		try {
			this.waitForElementToBePresent(this.textAccountDashboard, "Account Dashboard");
	    	assertTrue(isElementDisplayed(this.textAccountDashboard, "Account Dashboard"));
	    	
	    	this.click(this.textAccountDashboard, "Account Dashboard");
	    	
	    	defaultBillingAddressVerification();

	    	firstAndLastNameVerification();
	    	
	    	editAndPasswordButtonVerificationinContactInfo();
	    	
	    	getNewsLetterSubscriptionStatus();
	    	
	    	addressbookVerification();
	    	
	    	this.waitForElementToBePresent(this.textAccountInformationTab, "Account Information Tab");
	    	assertTrue(isElementDisplayed(this.textAccountInformationTab, "Account Information Tab"));
	    	
	    	this.click(this.textAccountInformationTab, "Account Information Tab");
	    	String URLAccountInformationTab = driver.getCurrentUrl();
	    	Assert.assertEquals(URLAccountInformationTab, "https://staging-marketplace.albertsons.com/customer/account/edit/" );
	    	
	    	firstNameLastNamePhoneVerification();
	    	
	    	this.waitForElementToBePresent(this.textNewsletterSubscription, "News letterSubscription");
	    	assertTrue(isElementDisplayed(this.textNewsletterSubscription, "News letterSubscription"));
	    	
	    	this.click(this.textNewsletterSubscription, "News letterSubscription");
	    	String URLNewsletterSubscription = driver.getCurrentUrl();
	    	
	    	wait(2);
			this.waitForElementToBeVisible(this.textNewsletterSubscriptionSave, "Subscription save");
			assertTrue(isElementDisplayed(this.textNewsletterSubscriptionSave, "Subscription save"));
			
			getNewsLetterSubscriptionSave();
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the logged in user name" + e.getMessage());
	        throw e;
	    }
	}
	
	public void firstAndLastNameVerification() throws Exception {
		try {
			
			this.waitForElementToBeVisible(this.textFirstAndLastName, "FirstAndLastName");
			assertTrue(isElementDisplayed(this.textFirstAndLastName, "FirstAndLastName"));
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the First and Lastname of the user" + e.getMessage());
	        throw e;
	    }
	}
	
	public void firstNameLastNamePhoneVerification() throws Exception {
		try {
			this.waitForElementToBeVisible(this.textFirstName, "First Name");
			assertTrue(isElementDisplayed(this.textFirstName, "First Name"));
			this.waitForElementToBeVisible(this.textLastName, "Last Name");
			assertTrue(isElementDisplayed(this.textLastName, "Last Name"));
//			this.waitForElementToBeVisible(this.textPhone, "Phone");
//			assertTrue(isElementDisplayed(this.textPhone, "Phone"));
			
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the First name Lastname  and phone number of the user" + e.getMessage());
	        throw e;
	    }
	}
	
	public void verifyManageAddressLink() throws Exception {
		try {
			this.waitForElementToBeVisible(this.manageAddress, "Manage Address Link");
			assertTrue(isElementDisplayed(this.manageAddress, "Manage Address Link"));
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the Manage address link from Address book" + e.getMessage());
	        throw e;
	    }
	}
	
	public void editAndPasswordButtonVerificationinContactInfo() throws Exception {
		try {
			this.waitForElementToBeVisible(this.editButtonInContactInfo, "Edit for contact info");
			assertTrue(isElementDisplayed(this.editButtonInContactInfo, "Edit for contact info"));
			this.waitForElementToBeVisible(this.passwordButtonInContactInfo, "Password for contact info");
			assertTrue(isElementDisplayed(this.passwordButtonInContactInfo, "Password for contact info"));
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the Edit and Password button in contact details" + e.getMessage());
	        throw e;
	    }
	}
	
	public void getNewsLetterSubscriptionStatus() throws Exception {
		try {
			this.waitForElementToBeVisible(this.textNewsletterSubscription, "Newsletter Subscription");
			assertTrue(isElementDisplayed(this.textNewsletterSubscription, "Newsletter Subscription"));
			
			this.waitForElementToBeVisible(this.editSubscriptionStatus, "Edit subscription status");
			assertTrue(isElementDisplayed(this.editSubscriptionStatus, "Edit subscription status"));
			
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get Newsletter subscription status  details" + e.getMessage());
	        throw e;
	    }
	}
	
	public void getNewsLetterSubscriptionSave() throws Exception {
		try {
			this.waitForElementToBeClickable(this.chkboxSubscription, "Checkbox Subscription");
			this.click(this.chkboxSubscription, "Checkbox Subscription");
			
			this.waitForElementToBeVisible(this.textNewsletterSubscriptionSave, "Subscription save");
			this.click(this.textNewsletterSubscriptionSave, "Subscription save");
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get Newsletter subscription status button" + e.getMessage());
	        throw e;
	    }
	}
	
	public void addressbookVerification() throws Exception {
		try {
//			driver.navigate().back();
			
			this.click(this.myAccount, "My Account");
//			this.waitForElementToBeVisible(this.labelMyAccount, "Label My Account");
			this.waitForElementToBeVisible(this.manageAddress, "Manage address link");
			assertTrue(isElementDisplayed(this.manageAddress, "Manage address link"));
			
			WebElement manageAddress = driver.findElement(By.xpath("//span[text()='Manage Addresses']"));
			JavascriptExecutor js = (JavascriptExecutor) driver; 
			js.executeScript("arguments[0].scrollTop = arguments[1];",manageAddress, 250);
			wait(5);
			
			this.click(this.manageAddress, "Manage address link");
			
			this.waitForElementToBePresent(this.textAddressBook, "Address Book");
	    	assertTrue(isElementDisplayed(this.textAddressBook, "Address Book"));
	    	this.click(this.textAddressBook, "Address Book");
	    	String URLAddressBook = driver.getCurrentUrl();
	    	wait(3);
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get Mange address link and contact details" + e.getMessage());
	        throw e;
	    }
	}
	
	public void defaultBillingAddressVerification() throws Exception {
		try {
			this.waitForElementToBePresent(this.textAddressBook, "Address Book");
	    	assertTrue(isElementDisplayed(this.textAddressBook, "Address Book"));
	    	this.click(this.textAddressBook, "Address Book");
	    	
//			this.waitForElementToBeVisible(this.manageAddress, "Manage address link");
//			assertTrue(isElementDisplayed(this.manageAddress, "Manage address link"));
			
			this.waitForElementToBeVisible(this.defaultBillingAddress, "Default billing address");
			assertTrue(isElementDisplayed(this.defaultBillingAddress, "Default billing address"));
			
			this.waitForElementToBeVisible(this.editBillingAddress, "Edit default billing address");
			assertTrue(isElementDisplayed(this.editBillingAddress, "Edit default billing address"));
			
			this.waitForElementToBeVisible(this.defaultShippingAddress, "Default shipping address");
			assertTrue(isElementDisplayed(this.defaultShippingAddress, "Default shipping address"));
			
			this.waitForElementToBeVisible(this.editShippingAddress, "Edit default shipping address");
			assertTrue(isElementDisplayed(this.editShippingAddress, "Edit default shipping address"));
			
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get Mange address link and contact details" + e.getMessage());
	        throw e;
	    }
	}
	public void verifyViewAllNavigateToMyOrders() throws Exception {
		try {
			WebElement scrollArea = driver.findElement(By.xpath("//span[text()='View All']"));
			JavascriptExecutor js = (JavascriptExecutor) driver; 
			js.executeScript("arguments[0].scrollTop = arguments[1];",scrollArea, 500);
			wait(4);
			scrollTo(this.txtAddressBook, "Address book");
			this.waitForElementToBeVisible(this.textViewAll, "View All");
			assertTrue(isElementDisplayed(this.textViewAll, "View All"));
			this.click(this.textViewAll, "View All");
			String URLMyorders = driver.getCurrentUrl();
	    	Assert.assertEquals(URLMyorders, "https://staging-marketplace.albertsons.com/sales/order/history/" );
		}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the viewall button" + e.getMessage());
	        throw e;
	    }
	}
	
    private void turnOffImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    
}
