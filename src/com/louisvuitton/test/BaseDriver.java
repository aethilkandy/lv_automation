package com.louisvuitton.test;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

/**
 * 
 * @author jvijayaratnam
 *
 */
public  class BaseDriver {
	protected static WebDriver driver;
	protected static AndroidDriver<AndroidElement> androidDriver;
	protected Logger logger;	
}
