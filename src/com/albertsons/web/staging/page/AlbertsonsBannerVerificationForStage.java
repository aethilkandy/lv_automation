package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for albertsons Homepage functionalities
*/
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetPageSource;
import org.testng.annotations.Test;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;

public class AlbertsonsBannerVerificationForStage extends ActionValidationLibrary {
    private static AlbertsonsBannerVerificationForStage albertsonsBannerVerificationForStage;

    public static Logger logger = Logger.getLogger(AlbertsonsBannerVerificationForStage.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Home Page Class
     *****************************************************************************************************************************************/
    
    By welcomeText =By.xpath("//div//span[contains(text(),'Welcome')]");
    
    By carrsBanner = By.xpath("//img[contains(@src,'carrs')]");
    
    By pavilionsBanner =By.xpath("//img[contains(@src,'pavilions')]");
    
    By randalsBanner = By.xpath("//img[contains(@src,'randals')]");
    
    By safewayBanner = By.xpath("//img[contains(@src,'safeway')]");
    
    By tomthumb = By.xpath("//img[contains(@src,'tomthumb')]");
    
    By vons = By.xpath("//img[contains(@src,'vons')]");
    
    By albertsons = By.xpath("//img[contains(@src,'albertsons')]");
    
    By jwelsco = By.xpath("//img[contains(@src,'jewel')]");
    
    By shaws = By.xpath("//img[contains(@src,'shaw')]");
    
    By startMarket = By.xpath("//img[contains(@src,'star')]");
    
    By acme = By.xpath("//img[contains(@src,'acme')]");

   private AlbertsonsBannerVerificationForStage() {

    }
   
   public static AlbertsonsBannerVerificationForStage getAlbersonBannerVerificationForStage() {

        if (AlbertsonsBannerVerificationForStage.albertsonsBannerVerificationForStage == null) {
            AlbertsonsBannerVerificationForStage.albertsonsBannerVerificationForStage = new AlbertsonsBannerVerificationForStage();
        }
        return AlbertsonsBannerVerificationForStage.albertsonsBannerVerificationForStage;
    }
    
   
	
    
    public void navigateToAlbertsonsHome() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsHome").get(0).get("URL")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsHome").get(0).get("URL");
    		}
	        Base.driver.get(URL);
	        wait(4);
	        waitForElementToBePresent(this.welcomeText, "Welcome text");
	        final boolean flag = Base.driver.findElement(this.welcomeText).isDisplayed();
	        assertTrue(flag);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to MarketPlace" + e.getMessage());
            throw e;
        }
    }
   
    public void navigateToAlbertsonsCarrsBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsCarrsBanner").get(0).get("URL_Carrs")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsCarrsBanner").get(0).get("URL_Carrs");
    		}
    		Base.driver.get(URL);
            final boolean flagCarrsBanner = Base.driver.findElement(this.carrsBanner).isDisplayed();
            assertTrue(flagCarrsBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to CarrsBanner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsPavilionsBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsPavilionsBanner").get(0).get("URL_Pavilions")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsPavilionsBanner").get(0).get("URL_Pavilions");
    		}
    		Base.driver.get(URL);
            final boolean flagPavilionsBanner = Base.driver.findElement(this.pavilionsBanner).isDisplayed();
            assertTrue(flagPavilionsBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Pavilions Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsRandallsBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsRandallsBanner").get(0).get("URL_Randalls")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsRandallsBanner").get(0).get("URL_Randalls");
    		}
    		Base.driver.get(URL);
            final boolean flagRandallsBanner = Base.driver.findElement(this.randalsBanner).isDisplayed();
            assertTrue(flagRandallsBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Pavilions Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsSafewayBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsSafewayBanner").get(0).get("URL_Safeway")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsSafewayBanner").get(0).get("URL_Safeway");
    		}
    		Base.driver.get(URL);
            final boolean flagSafewayBanner = Base.driver.findElement(this.safewayBanner).isDisplayed();
            assertTrue(flagSafewayBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Pavilions Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsTomthumbBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsTomthumbBanner").get(0).get("URL_Tomthumb")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsTomthumbBanner").get(0).get("URL_Tomthumb");
    		}
    		Base.driver.get(URL);
            final boolean flagTomthumbBanner = Base.driver.findElement(this.tomthumb).isDisplayed();
            assertTrue(flagTomthumbBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Tomthumb Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsVonsBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsVonsBanner").get(0).get("URL_Vons")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsVonsBanner").get(0).get("URL_Vons");
    		}
    		Base.driver.get(URL);
            final boolean flagVonsBanner = Base.driver.findElement(this.vons).isDisplayed();
            assertTrue(flagVonsBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Vons Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsAlbertsonsBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsAlbertsonsBanner").get(0).get("URL_Albertsons")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsAlbertsonsBanner").get(0).get("URL_Albertsons");
    		}
    		Base.driver.get(URL);
            final boolean flagAlbertsonsBanner = Base.driver.findElement(this.albertsons).isDisplayed();
            assertTrue(flagAlbertsonsBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Albertsons Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsJeweloscoBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsJeweloscoBanner").get(0).get("URL_Jewelsco")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsJeweloscoBanner").get(0).get("URL_Jewelsco");
    		}
    		Base.driver.get(URL);
            final boolean flagJwelscoBanner = Base.driver.findElement(this.jwelsco).isDisplayed();
            assertTrue(flagJwelscoBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Jwelsco Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsStarMarketBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsStarMarketBanner").get(0).get("URL_Star")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsStarMarketBanner").get(0).get("URL_Star");
    		}
    		Base.driver.get(URL);
            final boolean flagStarMarketBanner = Base.driver.findElement(this.startMarket).isDisplayed();
            assertTrue(flagStarMarketBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to StarMarket Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsShawsBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsShawsBanner").get(0).get("URL_Shaws")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsShawsBanner").get(0).get("URL_Shaws");
    		}
    		Base.driver.get(URL);
            final boolean flagShawsBanner = Base.driver.findElement(this.shaws).isDisplayed();
            assertTrue(flagShawsBanner);
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Shaws Banner" + e.getMessage());
            throw e;
        }
    }
    
    public void navigateToAlbertsonsAcmeMarketBanner() throws Exception {
    	String URL=null;
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsAcmeMarketBanner").get(0).get("URL_ACME")!=null){
    			 URL =(String)dataloader.getTestScenarios("MOREFORYOU.json","navigateToAlbertsonsAcmeMarketBanner").get(0).get("URL_ACME");
    		}
    		Base.driver.get(URL);
            final boolean flagAcmeMarketBanner = Base.driver.findElement(this.acme).isDisplayed();
            assertTrue(flagAcmeMarketBanner);
//            driver.quit();
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to Shaws Banner" + e.getMessage());
            throw e;
        }
    }
    
      
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

    
}
