package com.albertsons.web.staging.page;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 12/06/2018 | page object class for albertsons seller page functionality
*/
public class AlbertsonsSellerPage extends ActionValidationLibrary {
    private static AlbertsonsSellerPage albertsonsSellerPage;

    public static Logger logger = Logger.getLogger(AlbertsonsSellerPage.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Seller   Page Class
     *****************************************************************************************************************************************/
    By sellerStories = By.xpath("//a[contains(text(),'Seller Stories ')]");
    
    By addButton = By.xpath("//div[@class='block-title widget-title']/parent::div//div[@class='owl-wrapper']//div[@class='owl-item active'][1]//form");
        
    
   private AlbertsonsSellerPage() {

    }

    public static AlbertsonsSellerPage getAlbertsonsSellerPage() {

        if (AlbertsonsSellerPage.albertsonsSellerPage == null) {
            AlbertsonsSellerPage.albertsonsSellerPage = new AlbertsonsSellerPage();
        }
        return AlbertsonsSellerPage.albertsonsSellerPage;
    }
   
    public void clickOnSellerStories() throws Exception {
    	try {
    		this.waitForElementToBeVisible(this.sellerStories, "Seller Stories");
    		this.click(this.sellerStories, "Seller Stories");
    		boolean story =false;
    		String url = driver.getCurrentUrl();
    		if(url.contains("/story/")){
    			story=true;
    		}
    		assertTrue(story);
    		scrollToBottom();
    	}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::click on Seller Stories..." + e.getMessage());
            throw e;
        }
    }
    public void addProductToCart() throws Exception {
    	try {
    		this.waitForElementToBeVisible(this.addButton, "Add Button");
    		this.click(this.addButton, "Add Button");
    		wait(2);
    		scrollToTop();
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to MarketPlace" + e.getMessage());
            throw e;
        }
    }
   
    private void turnOffImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    
}
