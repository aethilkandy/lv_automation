package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for albertsons category landing page functionality
*/
public class AlbertsonsCategoryLanding extends ActionValidationLibrary {
    private static AlbertsonsCategoryLanding albertsonsCategoryLanding;

    public static Logger logger = Logger.getLogger(AlbertsonsCategoryLanding.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Category Landing  Page Class
     *****************************************************************************************************************************************/
    By category = By.xpath("//ul[@id='main-navigation-menu-ada']//li//a[@id='all-category-ada']");
    
    By txtGrocery =By.xpath("//ul[@aria-label='Categories']//a[text()='grocery']");
    
    By txtCandyAndChocolate = By.xpath("//ul[@aria-label='grocery']//a[text()='candy & chocolate']");
    
    By filterByBrand = By.xpath("//button[text()='Filter By Brand']");
    
    By chkAlterEco =By.xpath("//span[text()='Alter Eco']//parent::a");
    
    By chkPrice = By.xpath("//ol[@class='items am-filter-items-attr_offer_min_price']//li[2]");
    
    By firstProduct = By.xpath("//ol[@class='products list items product-items itemgrid']//li[1]//img");
    
    By soldBy =By.xpath("//span[@class='offer-shop-name']");
    
    By filteredNoOfProducts =By.xpath("//p[@class='toolbar-amount']//span[3]");
    
    By noOfItems =By.xpath("//div[@id='amasty-shopby-product-list']//div[@class='toolbar toolbar-products'][1]//p//span");
    
    By removePriceFilter = By.xpath("//a[contains(@title, 'Remove search filter Price')]");
    
    By removeBrandFilter = By.xpath("//a[contains(@title, 'Remove search filter Brand')]");
    
    By addButton = By.xpath("//button[@title='Add']");
    
        
    
   private AlbertsonsCategoryLanding() {

    }

    public static AlbertsonsCategoryLanding getAlbersonsCategoryLanding() {

        if (AlbertsonsCategoryLanding.albertsonsCategoryLanding == null) {
            AlbertsonsCategoryLanding.albertsonsCategoryLanding = new AlbertsonsCategoryLanding();
        }
        return AlbertsonsCategoryLanding.albertsonsCategoryLanding;
    }
   
    public void verifyCategoryLandingPage() throws Exception {
    	try {
    		this.waitForElementToBeVisible(this.category, "category");
    		this.mouseOver(this.category, "category");
    		this.waitForElementToBePresent(this.txtGrocery, "Grocery");
    		this.mouseOver(this.txtGrocery, "Grocery");
    		this.waitForElementToBePresent(this.txtCandyAndChocolate, "Candy and chocolate");
    		this.click(this.txtCandyAndChocolate, "candy and chocolate");
    		
//    		if(this.isElementDisplayed(this.filterByBrand,"Filter by brand")){
////    		this.waitForElementToBePresent(this.filterByBrand, "Filter By Brand");
//    		this.click(this.filterByBrand, "Filter by brand");
//    		this.waitForElementToBePresent(this.chkAlterEco, "Alter Eco");
//    		this.click(this.chkAlterEco,"Alter Eco");
//    		}
    	
//    		scrollTo(this.chkPrice, "Check price");
//    		if(this.isElementDisplayedAfterSpecificTimeOut(this.chkPrice,"Filter by price",10)){
//    		this.waitForElementToBeClickable(this.chkPrice, "price");
//    		this.click(this.chkPrice, "price");
//    		}
//    		scrollToTop();
//    		wait(10);
    		int iCount = driver.findElements(By.xpath("//button[@class='action tocart primary']")).size();
    		System.out.println("iCount---->"+iCount);
    		if(iCount>0){
    			addProductToCart();
    		}else{
    			scrollToBottom();
    			wait(10);
    			this.click(this.removePriceFilter, "remove price filter");
    			if(driver.findElements(By.xpath("//button[@class='action tocart primary']")).size()>0){
    			addProductToCart();
    			}else{
    				scrollToBottom();
        			wait(10);
        			this.click(this.removeBrandFilter, "remove brand filter");
        			addProductToCart();
    			}
    		}
    	}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Hero Banner is not visible" + e.getMessage());
            throw e;
        }
    }
    public void addProductToCart() throws Exception {
    	try {
    		System.out.println("value---->"+driver.findElements(By.xpath("//button[@title='Add']")).size());
    		for(int i =1;i<4;i++){
    			System.out.println("resched in loop");
    			wait(5);
    			WebElement item = driver.findElement(By.xpath("//ol[@class='products list items product-items itemgrid']//li["+i+"]//img"));
    			System.out.println("clicking element.........");
    			item.click();
//    			WebElement brand = driver.findElement(By.xpath("//th[text()='Brand']//following-sibling::td"));
//    			assertEquals("Alter Eco", brand.getText());
    			//scrollToBottom();
    				WebElement add = driver.findElement(By.xpath("//button[@title='Add']"));
    				System.out.println("##########before add");
    				add.click();
//    				wait(60);
    				//scrollToTop();
    				System.out.println("##########before navigate");
    				driver.navigate().back();
    				System.out.println("##########after navigate");
    			driver.navigate().back();
//    			wait(60);
    		}
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::Nativagation to MarketPlace" + e.getMessage());
            throw e;
        }
    }
   
    private void turnOffImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    
}
