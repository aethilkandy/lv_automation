package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 06/11/2019 | page object class for Louisvuitton Order Details functionality
*/
public class LouisvuittonOrderDetails extends ActionValidationLibrary {
    private static LouisvuittonOrderDetails louisvuittonOrderDetails;

    public static Logger logger = Logger.getLogger(LouisvuittonOrderDetails.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Louisvuitton OrderDetails Page Class
     *****************************************************************************************************************************************/
    By txtTanksTitle =By.xpath("//span[@class='thanksTitleLabel']");
    
    By txtOrderNumber = By.xpath("//div[@class='orderNumber']");
    
    By txtCustomerName = By.xpath("//span[@class='thanksPageLabel']");
    
    By tabDeliveryDetails = By.xpath("//ul[@id='clientInfo']//button[@data-detail='delivery']");
    
    By tabPaymentInfo = By.xpath("//ul[@id='clientInfo']//button[@data-detail='payment']");
    
    By tabExchangeAndReturn = By.xpath("//ul[@id='clientInfo']//button[@data-detail='returnexchange']");
    
    By tabClientService = By.xpath("//ul[@id='clientInfo']//button[@data-detail='customer_service']");
    
    By btnTrackOrder = By.xpath("//div[@class='thanks-buttons']//a[1]");
    
    By btnBackToHome = By.xpath("//div[@class='thanks-buttons']//a[2]");
    
   LouisvuittonOrderDetails() {

    }

    public static LouisvuittonOrderDetails getLouisvuittonOrderDetails() {
    	
        if (LouisvuittonOrderDetails.louisvuittonOrderDetails == null) {
            LouisvuittonOrderDetails.louisvuittonOrderDetails = new LouisvuittonOrderDetails();
        }
        return LouisvuittonOrderDetails.louisvuittonOrderDetails;
    }
    
	public void verifyOrderConfirmationMessage() throws Exception {
		try {
			this.waitForElementToBeVisible(this.txtCustomerName, "Customer Name");
			assertNotNull(this.txtCustomerName);
			this.waitForElementToBeVisible(this.txtOrderNumber, "Order Number");
			assertNotNull(this.txtOrderNumber);
			this.waitForElementToBeVisible(this.txtTanksTitle, "Thanks Message");
			assertNotNull(this.txtTanksTitle);
			this.waitForElementToBeVisible(this.tabDeliveryDetails, "Delivery detailstab");
			assertNotNull(this.tabDeliveryDetails);
			this.waitForElementToBeVisible(this.tabPaymentInfo, "Payment Information");
			assertNotNull(this.tabPaymentInfo);
			this.waitForElementToBeVisible(this.tabExchangeAndReturn, "Exchange and return");
			assertNotNull(this.tabExchangeAndReturn);
			this.waitForElementToBeVisible(this.tabClientService, "Client Service");
			assertNotNull(this.tabClientService);
			}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the order order confirmation message" + e.getMessage());
	        throw e;
	    }
	}
	
	
    private void turnOffImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    
}
