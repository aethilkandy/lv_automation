package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for albertsons Homepage functionalities
*/
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetPageSource;
import org.testng.annotations.Test;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;

public class AlbertsonsDiscover extends ActionValidationLibrary {
    private static AlbertsonsDiscover albertsonsDiscoverPage;

    public static Logger logger = Logger.getLogger(AlbertsonsDiscover.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Home Page Class
     *****************************************************************************************************************************************/
    
    By headerContainer = By.xpath("//header[@id='header-container']");
    
    
   private AlbertsonsDiscover() {

    }
   
   public static AlbertsonsDiscover getAlbersonHomePage() {

        if (AlbertsonsDiscover.albertsonsDiscoverPage == null) {
            AlbertsonsDiscover.albertsonsDiscoverPage = new AlbertsonsDiscover();
        }
        return AlbertsonsDiscover.albertsonsDiscoverPage;
    }
    
   public void verifyHiGuest() throws Exception {
   	try {
//   		this.waitForElementToBeVisible(this.txtHiGuest, "Hi Guest");
//   		this.click(this.txtHiGuest, "Hi Guest");
//   		this.click(this.txtHiGuest, "Hi Guest");
//   		final boolean flagtPassword = Base.driver.findElement(this.txtPassword).isDisplayed();
//	        assertTrue(flagtPassword);
   		}
   	catch (final Exception e) {
           ReporterListener.logFailureMessage("Failed ::signup page is not visible" + e.getMessage());
           throw e;
       }
   }
	
    
    
    
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

    
}
