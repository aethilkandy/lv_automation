package com.louisvuitton.test;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.albertsons.web.staging.page.LouisvuittonCheckout;
import com.albertsons.web.staging.page.LouisvuittonHome;
import com.albertsons.web.staging.page.LouisvuittonMinicart;
import com.albertsons.web.staging.page.LouisvuittonOrderDetails;
import com.albertsons.web.staging.page.LouisvuittonShipping;

public class GuestUserCheckoutflowTest extends BaseTest{
	
	protected LouisvuittonHome louisvuittonHomePage;
	protected LouisvuittonShipping louisvuittonShipping;
	protected LouisvuittonMinicart louisvuittonMinicart;
	protected LouisvuittonCheckout louisvuittonCheckout;
	protected LouisvuittonOrderDetails louisvuittonOrderDetails;
 
	@BeforeClass
	
	public void initialize() {
		this.louisvuittonHomePage=LouisvuittonHome.getLouisvuittonHomePage();
		this.louisvuittonMinicart=LouisvuittonMinicart.getAlbersonMinicart();
		this.louisvuittonShipping =LouisvuittonShipping.getLouisvuittonShipping();
		this.louisvuittonCheckout =LouisvuittonCheckout.getLouisvuittonCheckout();
		this.louisvuittonOrderDetails = LouisvuittonOrderDetails.getLouisvuittonOrderDetails();
	}	

	 @Test(priority=1)
	    public void navigateToLouisvuittonHome() throws Exception {
	        try {
	        	test =extent.createTest("navigateToLouisvuittonHome");
	            this.louisvuittonHomePage.navigateToLouisvuittonHome();
	        } catch (final Exception e) {
	            e.printStackTrace();
	            Assert.fail();
	        }
	    }
	
	 @Test(priority=2)
	 public void verifySearchAndFilter() throws Exception {
	     try {
	    	 String product = "M41414";
	     	test =extent.createTest("verifySearchAndFilter");
	         this.louisvuittonHomePage.verifySearchAndFilter(product);
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=3)
	 public void addToCart() throws Exception {
	     try {
	    	 boolean flag = true;
	     	test =extent.createTest("addToCart");
	         this.louisvuittonHomePage.addToCart(flag);
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=4)
	 public void clickToCart() throws Exception {
	     try {
	    	 boolean flag = false;
	     	test =extent.createTest("clickToCart");
	         this.louisvuittonHomePage.clickToCart(flag);
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=5)
	 public void clickToViewCartDetails() throws Exception {
	     try {
	     	test =extent.createTest("clickToViewCartDetails");
	         this.louisvuittonHomePage.clickToViewCartDetails();
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=6)
	 public void verifyProceedToCheckout() throws Exception {
	     try {
	    	 boolean flag = false;
	     	test =extent.createTest("verifyProceedToCheckout");
	         this.louisvuittonMinicart.verifyProceedToCheckout(flag);
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=7)
	 public void verifyContinueWithoutAccount() throws Exception {
	     try {
	    	 boolean flag = true;
	     	test =extent.createTest("verifyContinueWithoutAccount");
	         this.louisvuittonMinicart.verifyContinueWithoutAccount(flag);
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=8)
	 public void verifyBillingAddress() throws Exception {
	     try {
	    	 Boolean flag= true;
	     	test =extent.createTest("verifyBillingAddress");
	         this.louisvuittonShipping.verifyBillingAddress(flag);
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=9)
	 public void verifyProceedToBilling() throws Exception {
	     try {
	    	 Boolean flag= true;
	     	test =extent.createTest("verifyProceedToBilling");
	         this.louisvuittonShipping.verifyProceedToBilling(flag);
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=10)
	 public void verifyCardDetails() throws Exception {
	     try {
	     	test =extent.createTest("verifyCardDetails");
	         this.louisvuittonCheckout.verifyCardDetails();
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=11)
	 public void clickOnProceedToCheckout() throws Exception {
	     try {
	     	test =extent.createTest("clickOnProceedToCheckout");
	         this.louisvuittonCheckout.clickOnProceedToCheckout();
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=12)
	 public void verifySubmitOrder() throws Exception {
	     try {
	     	test =extent.createTest("verifySubmitOrder");
	         this.louisvuittonCheckout.verifySubmitOrder();
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	 
	 @Test(priority=13)
	 public void verifyOrderConfirmationMessage() throws Exception {
	     try {
	     	test =extent.createTest("verifyOrderConfirmationMessage");
	         this.louisvuittonOrderDetails.verifyOrderConfirmationMessage();
	     } catch (final Exception e) {
	         e.printStackTrace();
	         Assert.fail();
	     }
	 }
	
 }



