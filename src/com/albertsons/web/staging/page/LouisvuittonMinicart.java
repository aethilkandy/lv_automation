package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;

/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for louisvuitton Minicart functionalities
*/
public class LouisvuittonMinicart extends ActionValidationLibrary {
	private static LouisvuittonMinicart louisvuittonMinicart;

	public static Logger logger = Logger.getLogger(LouisvuittonMinicart.class.getName());

	/******************************************************************************************************************************************
	 * Page objects for Louisvuitton Minicart Page Class
	 *****************************************************************************************************************************************/

	By btnProceedToCheckout = By.xpath("//button[@id='proceedToCheckoutButtonTop']");
	
	By txtHeader = By.xpath("//h1[@class='titleIdentification']");
	
	By btnContinueWithoutAccount = By.xpath("//a[@id='continueWithoutLogging']");
	
	By txtDeliveryOptions = By.xpath("//h1[@class='ccSectionTitle']");
	
	By chkCollectInStore = By.xpath("//input[@id='ccInput']");
	
	By mapTitle = By.xpath("//h2[@class='ccMaptitle']");
	
	By chkShop = By.xpath("//div[@id='storeListWraper']//div[@data-store='I03']");
	
	By txtEmail = By.xpath("//input[@id='email']");
	
	By btnProceedToBilling = By.xpath("//input[@id='globalSubmit']");
	
	By sampleProduct = By.xpath("//div[@class='sample-block']//ul[@summary='item details']//li[1]//input[@type='checkbox']");
	
	LouisvuittonMinicart() {

	}

	public static LouisvuittonMinicart getAlbersonMinicart() {

		if (LouisvuittonMinicart.louisvuittonMinicart == null) {
			LouisvuittonMinicart.louisvuittonMinicart = new LouisvuittonMinicart();
		}
		return LouisvuittonMinicart.louisvuittonMinicart;
	}
	
	 public void verifyProceedToCheckout(boolean flag) throws Exception {
	    	try {
	    		if(flag == true){
	    			this.waitForElementToBePresent(this.sampleProduct, "sample product");
	    			this.click(this.sampleProduct, "sample product");
	    		}
	    		scrollToTop();
	    		this.waitForElementToBePresent(this.btnProceedToCheckout, "Proceed to Checkout");
	    		this.click(this.btnProceedToCheckout, "Proceed to Checkout");
	    		this.waitForElementToBePresent(this.txtHeader, "Identification");
	    		final boolean flagIdenfication = Base.driver.findElement(this.txtHeader).isDisplayed();
				assertTrue(flagIdenfication);
	    		}
	    	catch (final Exception e) {
	            ReporterListener.logFailureMessage("Failed ::to load checkout page" + e.getMessage());
	            throw e;
	        }
	    }
	 
	 public void verifyContinueWithoutAccount(boolean flag) throws Exception {
	    	try {
	    		this.waitForElementToBePresent(this.btnContinueWithoutAccount, "Continue without Logging");
	    		this.click(this.btnContinueWithoutAccount, "Continue without Logging");
	    		if(flag== true){
	    		this.waitForElementToBePresent(this.txtDeliveryOptions, "Delivery Options");
	    		final boolean flagDeliveryOptions = Base.driver.findElement(this.txtDeliveryOptions).isDisplayed();
				assertTrue(flagDeliveryOptions);
	    		}
	    		}
	    	catch (final Exception e) {
	            ReporterListener.logFailureMessage("Failed ::to load 'Continue without account' option button" + e.getMessage());
	            throw e;
	        }
	    }
	 
	 public void verifyCollectinStore() throws Exception {
	    	try {
	    		this.waitForElementToBePresent(this.chkCollectInStore, "Collect in store");
	    		this.click(this.chkCollectInStore, "Collect in store"); 
	    		this.waitForElementToBePresent(this.mapTitle, "Map title");
	    		final boolean flagMapTitle = Base.driver.findElement(this.mapTitle).isDisplayed();
				assertTrue(flagMapTitle);
	    		}
	    	catch (final Exception e) {
	            ReporterListener.logFailureMessage("Failed ::to load MapTitle" + e.getMessage());
	            throw e;
	        }
	    }
	 
	 public void verifyStoreSelection() throws Exception {
	    	try {
	    		DataLoader dataloader =new DataLoader();
	    		this.waitForElementToBePresent(this.chkShop, "Select shop");
	    		this.click(this.chkShop, "Select shop"); 
	    		scrollToBottom();
	    		String Email_Id =null;    		
	    		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Email")!=null){
	    			Email_Id  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","verifyBillingAddress").get(0).get("Email");
	    			
	    		}
	    		driver.findElement(this.txtEmail).sendKeys(Email_Id);
	    		this.waitForElementToBePresent(this.btnProceedToBilling, "Proceed to billing");
	    		this.click(this.btnProceedToBilling, "Proceed to billing");
	    		final boolean flagMapTitle = Base.driver.findElement(this.mapTitle).isDisplayed();
				assertTrue(flagMapTitle);
	    		}
	    	catch (final Exception e) {
	            ReporterListener.logFailureMessage("Failed ::to select collection store from list" + e.getMessage());
	            throw e;
	        }
	    }
	
	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

}
