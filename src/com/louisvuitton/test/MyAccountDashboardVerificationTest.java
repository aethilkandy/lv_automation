package com.louisvuitton.test;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.albertsons.web.staging.page.LouisvuittonCheckout;
import com.albertsons.web.staging.page.AlbertsonsGuestAddress;
import com.albertsons.web.staging.page.LouisvuittonHome;
import com.albertsons.web.staging.page.AlbertsonsHomeFooter;
import com.albertsons.web.staging.page.AlbertsonsMyAccount;
import com.albertsons.web.staging.page.AlbertsonsMyOrder;
import com.albertsons.web.staging.page.LouisvuittonOrderDetails;
import com.albertsons.web.staging.page.LouisvuittonShipping;
import com.albertsons.web.staging.page.LouisvuittonSignUp;
import com.albertsons.web.staging.page.LouisvuittonSignIn;

public class MyAccountDashboardVerificationTest extends BaseTest{
	
	protected LouisvuittonHome louisvuittonHome;
	protected LouisvuittonSignUp louisvuittonSignUp;
	protected LouisvuittonSignIn louisvuittonSignIn;
	protected AlbertsonsMyAccount albertsonsMyAccount;
	protected AlbertsonsMyOrder albertsonsMyOrder;
	protected LouisvuittonCheckout albertsonsCheckout;
	protected LouisvuittonShipping albertsonsShipping; 
	protected LouisvuittonOrderDetails albertsonsOrderDetails;
	protected AlbertsonsGuestAddress albertsonsGuestAddress;
	protected AlbertsonsHomeFooter albertsonsFooter;
		
	@BeforeClass
	public void initialize() {
		this.louisvuittonHome=LouisvuittonHome.getLouisvuittonHomePage();
		this.louisvuittonSignUp =LouisvuittonSignUp.getLouisvuittonSignUp();
		this.louisvuittonSignIn=louisvuittonSignIn.getLouisvuittonSignIn();
		this.albertsonsMyAccount =AlbertsonsMyAccount.getAlbersonsMyAccount();
		this.albertsonsMyOrder =AlbertsonsMyOrder.getAlbersonsMyOrder();
		this.albertsonsCheckout =LouisvuittonCheckout.getLouisvuittonCheckout();
		this.albertsonsShipping =LouisvuittonShipping.getLouisvuittonShipping();
		this.albertsonsOrderDetails =LouisvuittonOrderDetails.getLouisvuittonOrderDetails();
		this.albertsonsGuestAddress =AlbertsonsGuestAddress.getAlbersonsGuestAddress();
		this.albertsonsFooter = AlbertsonsHomeFooter.getAlbertsonsHomeFooterPage();
		
	}	
	
	 @Test(priority=1)
	    public void navigateToLouisvuittonHome() throws Exception {
	        try {
	        	test =extent.createTest("navigateToLouisvuittonHome");
	            this.louisvuittonHome.navigateToLouisvuittonHome();
	        } catch (final Exception e) {
	            e.printStackTrace();
	            Assert.fail();
	        }
	   }
	    
    @Test(priority=2)
    public void verifyMyLvButton() throws Exception {
        try {
        	test =extent.createTest("verifyMyLvButton");
            this.louisvuittonHome.verifyMyLvButton();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=3)
    public void louisvuittonSignInVerification() throws Exception {
        try {
        	test =extent.createTest("louisvuittonSignInVerification");
        	this.louisvuittonSignIn.louisvuittonSignInVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
   
//    @Test(priority=4)
//    public void verifyMyDashboard() throws Exception {
//        try {
//        	test =extent.createTest("verifyMyDashboard");
//        	this.albertsonsMyAccount.verifyMyDashboard();
//        } catch (final Exception e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }
    
//    @Test(priority=5)
//    public void navigateToMyAccount() throws Exception {
//        try {
//        	test =extent.createTest("navigateToMyAccount");
//        	this.albertsonsMyAccount.navigateToMyAccount();
//        } catch (final Exception e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }
    
//    @Test(priority=6)
//    public void getVerifyAccountTabDetails() throws Exception {
//        try {
//        	test =extent.createTest("getVerifyAccountTabDetails");
//            this.albertsonsMyAccount.getVerifyAccountTabDetails();
//        } catch (final Exception e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }
//    @Test
//    (priority=7)
//    public void verifyViewAllNavigateToMyOrders() throws Exception {
//        try {
//        	test =extent.createTest("verifyViewAllNavigateToMyOrders");
//            this.albertsonsMyAccount.verifyViewAllNavigateToMyOrders();
//        } catch (final Exception e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }
//    
//    @Test(priority=8)
//    public void getMyorderHistoryVerification() throws Exception {
//        try {
//        	test =extent.createTest("getMyorderHistoryVerification");
//            this.albertsonsMyOrder.getMyorderHistoryVerification();
//        } catch (final Exception e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }
//
// @Test(priority=9)
//    public void getVerifyPaginationAndOrder() throws Exception {
//        try {
//        	test =extent.createTest("getVerifyPaginationAndOrder");
//            this.albertsonsMyOrder.getVerifyPaginationAndOrder();
//        } catch (final Exception e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }  
// 
// @Test(priority=10)
//    public void verifyGoBackFunction() throws Exception {
//        try {
//        	test =extent.createTest("verifyGoBackFunction");
//            this.albertsonsMyOrder.verifyGoBackFunction();
//        } catch (final Exception e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }      
 }



