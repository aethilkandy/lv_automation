package com.louisvuitton.test;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.albertsons.web.staging.page.LouisvuittonCheckout;
import com.albertsons.web.staging.page.LouisvuittonHome;
import com.albertsons.web.staging.page.LouisvuittonMinicart;
import com.albertsons.web.staging.page.AlbertsonsMyAccount;
import com.albertsons.web.staging.page.AlbertsonsMyOrder;
import com.albertsons.web.staging.page.LouisvuittonShipping;
import com.albertsons.web.staging.page.LouisvuittonSignIn;
import com.albertsons.web.staging.page.LouisvuittonSignUp;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@Test
public class AlbertsonsPageBase extends ActionValidationLibrary{
	
	protected static ExtentTest test;
	
	protected LouisvuittonHome albertsonsHome;
	
	protected LouisvuittonSignUp albertsonsSignUp;
	
	protected AlbertsonsMyAccount albertsonsMyaccount;
	
	protected AlbertsonsMyOrder albertsonsMyOrder;
	
	protected LouisvuittonSignIn albertsonsSignIn;
	
	protected LouisvuittonCheckout albertsonsCheckout;
	
	protected LouisvuittonMinicart albertsonsMinicart;
	
	protected LouisvuittonShipping albertsonsShipping;  

    @BeforeClass
    public void initialize() {
    	
    	this.albertsonsHome = LouisvuittonHome.getAlbersonHomePage();
    	this.albertsonsSignIn =LouisvuittonSignIn.getAlbersonsSignIn();
    	this.albertsonsMyaccount =AlbertsonsMyAccount.getAlbersonsMyAccount();
    	this.albertsonsMyOrder =AlbertsonsMyOrder.getAlbersonsMyOrder();
    	this.albertsonsCheckout =LouisvuittonCheckout.getAlbersonCheckout();
    	this.albertsonsSignUp=LouisvuittonSignUp.getAlbersonsSignUp();
    	this.albertsonsMinicart=LouisvuittonMinicart.getAlbersonMinicart();
    	this.albertsonsShipping =LouisvuittonShipping.getAlbertsonsShipping();
    	
    }
    
    @BeforeTest
    public void beforeTest(String browserName) {
    	
		albertsonsHome = PageFactory.initElements(driver, LouisvuittonHome.class);
    	albertsonsSignIn = PageFactory.initElements(driver, LouisvuittonSignIn.class);
    	albertsonsMyaccount =PageFactory.initElements(driver,AlbertsonsMyAccount.class);
    	albertsonsMyOrder =PageFactory.initElements(driver,AlbertsonsMyOrder.class);
    	albertsonsCheckout =PageFactory.initElements(driver,LouisvuittonCheckout.class);
    	albertsonsSignUp=PageFactory.initElements(driver,LouisvuittonSignUp.class);
    	albertsonsMinicart=PageFactory.initElements(driver,LouisvuittonMinicart.class);
    	albertsonsShipping =PageFactory.initElements(driver,LouisvuittonShipping.class);
    }
    
    @AfterTest
    public void afterTest() {
     System.out.println("in after test");
     albertsonsHome = null;
     albertsonsSignIn = null;
 	 albertsonsMyaccount =null;
 	 albertsonsMyOrder =null;
 	 albertsonsCheckout =null;
 	 albertsonsSignUp=null;
 	 albertsonsMinicart=null;
 	 albertsonsShipping =null;
    }

    @BeforeMethod
    public void start() throws Exception {
        System.out.println("In Start");
    }
    @Test
    public void getHomePageFunctionalities() throws Exception {
        try {
//        	test = extent.getResult(result);
            this.albertsonsHome.navigateToAlbertsonsAdminPage();
            this.albertsonsHome.navigateToAlbertsonsHome();
            this.albertsonsHome.albertsonsLogoVerification();
            this.albertsonsHome.getHeroBannerVerification();
            this.albertsonsHome.getHoverdataFromEyebrowHeader();
            this.albertsonsHome.eyebrowHeaderAndMegamenuVerification();
            this.albertsonsHome.footerLayoutVerification();
            test.log(LogStatus.INFO, "the test tours is passed");
            
          //  this.albertsonsHome.getNewArrivalsVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void albertsonsSignInPage() throws Exception {
        try {
        	System.out.println("sign in page");
            this.albertsonsSignIn.albertsonsSignIn();
         //   this.albertsonsSignIn.getWelcomeGuestName();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void albertsonsMyAccountPage() throws Exception {
        try {
        	System.out.println("sign in page");
//            this.albertsonsMyaccount.navigateToMyAccount();
            this.albertsonsMyaccount.getVerifyAccountTabDetails();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void albertsonsMyOrder() throws Exception {
        try {
        	System.out.println("albertsonsMyOrder");
            this.albertsonsMyOrder.getMyorderHistoryVerification();
            this.albertsonsMyOrder.getVerifyPaginationAndOrder();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void albertsonsMinicart() throws Exception {
        try {
        	System.out.println("albertsonsMinicart");
            this.albertsonsMinicart.doDeleteFromCrat();
//            this.albertsonsMyOrder.getVerifyPaginationAndOrder();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void albertsonsCheckoutFlow() throws Exception {
        try {
            this.albertsonsCheckout.albersonsCheckoutFlow();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void albertsonsShippingFunction() throws Exception {
        try {
            this.albertsonsShipping.getCheckoutItem();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
    
