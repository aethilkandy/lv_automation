package com.louisvuitton.test;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import java.io.IOException;
import org.openqa.selenium.TakesScreenshot;
import org.apache.commons.io.FileUtils;

public class BaseTest {

	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	WebDriver driver;

	@BeforeSuite
	public void setUp() {
		htmlReporter = new ExtentHtmlReporter(
				"C:/Users/apattali/Projects/LV_automation/test-output/LVAutomation.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setReportName("Louis Vuitton Automation Report");
		extent.setSystemInfo("Suite Name : ", "AlbertsonsTestSuite");
		extent.setSystemInfo("OS : ", "Windows 10");
		extent.setSystemInfo("Browser Name : ", "Chrome");
		extent.setSystemInfo("Execution Mode : ", "local");
		extent.setSystemInfo("Test Url : ", "https://Vuitton:5xaQqMK854@en-ppe.louisvuitton.com/eng-nl/homepage?dispatchCountry=BE");

	}

	@AfterMethod
	public void getResult(ITestResult result) throws IOException {
		{
			if (result.getStatus() == ITestResult.FAILURE) {
				test.fail(MarkupHelper.createLabel(result.getName() + "Test case failed", ExtentColor.RED));
				test.fail(result.getThrowable());
			} else if (result.getStatus() == ITestResult.SUCCESS) {
				test.pass(MarkupHelper.createLabel(result.getName() + "Test case passed", ExtentColor.GREEN));
			} else {
				test.skip(MarkupHelper.createLabel(result.getName() + "Test case skiped", ExtentColor.YELLOW));

			}
		}
	}

	@AfterSuite
	public void tearDown() {
		extent.flush();
	}

	public String captureScreen() throws IOException {
		System.out.println("screenshot screen............");
		TakesScreenshot screen = (TakesScreenshot) driver;
		File src = screen.getScreenshotAs(OutputType.FILE);
		String dest = "C://Automation_Reports//screenshots//" + System.currentTimeMillis() + ".png";
		File target = new File(dest);
		FileUtils.copyFile(src, target);
		return dest;
	}

}