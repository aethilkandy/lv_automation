package com.albertsons.web.staging.page;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 08/03/2018 | page object class for albertsons signup functionality
*/
public class AlbertsonsGuestAddress extends ActionValidationLibrary {
    private static AlbertsonsGuestAddress albertsonsGuestAddress;

    public static Logger logger = Logger.getLogger(AlbertsonsGuestAddress.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons Guest Address  Page Class
     *****************************************************************************************************************************************/
    By emailId =By.xpath("//input[@placeholder='Enter Email Id']");
    
    By firstName = By.xpath("//input[@name='firstname']");
    
    By lastName =By.xpath("//input[@name='lastname']");
    
    By addressLineOne = By.xpath("//input[@name='street[0]']");
    
    By city =By.xpath("//input[@name='city']");
    
    By regionId =By.xpath("//select[@name='region_id']");
    
    By optionCalifornia =By.xpath("//option[text()='California']");
    
    By zipCode = By.xpath("//input[@name='postcode']");
    
    By telephone =By.xpath("//input[@name='telephone']");
    
    By btnNext = By.xpath("//span[text()='Next']");
    
    By addressValidation = By.xpath("//div//ul[1]//input[@name='candidate']");
    
    By addressVAlidationTwo = By.xpath("//div//ul[1]//input[@name='candidate']");
    
    By btnContinue = By.xpath("//span[text()='Continue']");
    
    
   private AlbertsonsGuestAddress() {

    }

    public static AlbertsonsGuestAddress getAlbersonsGuestAddress() {

        if (AlbertsonsGuestAddress.albertsonsGuestAddress == null) {
            AlbertsonsGuestAddress.albertsonsGuestAddress = new AlbertsonsGuestAddress();
        }
        return AlbertsonsGuestAddress.albertsonsGuestAddress;
    }
   
    public void shippingAddressValidation() throws Exception {
    	try {
    		DataLoader dataloader =new DataLoader();
    		if((String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("Email_Id")!=null){
    		String	Email_Id =(String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("Email_Id");
    		wait(3);
    		 this.waitForElementToBeVisible(this.emailId, "Email Id");
    		 driver.findElement(this.emailId).sendKeys(Email_Id);
    		 String	First_Name =(String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("First_Name"); 
    		 this.waitForElementToBeVisible(this.firstName, "First Name");
    		 driver.findElement(this.firstName).sendKeys(First_Name);
    		 this.waitForElementToBeVisible(this.lastName, "Last Name");
    		 String	Last_Name =(String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("Last_Name"); 
    		 driver.findElement(this.lastName).sendKeys(Last_Name);
    		 String	Address_Line_One =(String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("Address_Line_One"); 
    		 this.waitForElementToBeVisible(this.addressLineOne, "Address Line");
    		 driver.findElement(this.addressLineOne).sendKeys(Address_Line_One);
    		 String	City =(String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("City");
    		 this.waitForElementToBeVisible(this.city, "City");
    		 driver.findElement(this.city).sendKeys("California");
    		 this.waitForElementToBeVisible(this.regionId, "Region");
    		 this.click(this.regionId, "Region");
    		 this.waitForElementToBePresent(this.optionCalifornia, "California");
    		 this.click(this.optionCalifornia, "california");
    		 String	Zipcode =(String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("Zipcode");
    		 this.waitForElementToBePresent(this.zipCode, "Zipcode");
    		 driver.findElement(this.zipCode).sendKeys(Zipcode);
    		 String	Telephone =(String)dataloader.getTestScenarios("MOREFORYOU.json","shippingAddressValidation").get(0).get("Telephone");
    		 this.waitForElementToBePresent(this.telephone, "telephone");
    		 driver.findElement(this.telephone).sendKeys("8002791455");
    		}
    		 this.waitForElementToBePresent(this.btnNext, "Next");
    		 this.click(this.btnNext, "Next");
    		 wait(7);
    		 if(this.isElementDisplayed(this.addressVAlidationTwo, "Address Validation Two")){
    			 this.click(this.addressVAlidationTwo, "Address Validation Two");
    		 }else{
    		 this.waitForElementToBePresent(this.addressValidation, "Address Validation");
    		 this.click(this.addressValidation, "Address Validation");
    		 }
    		 this.waitForElementToBePresent(this.btnContinue, "continue");
    		 this.click(this.btnContinue, "Continue");
    		}
    	catch (final Exception e) {
            ReporterListener.logFailureMessage("Failed ::to see SignUp button" + e.getMessage());
            throw e;
        }
    }  	
    
   
    private void turnOffImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    
}
