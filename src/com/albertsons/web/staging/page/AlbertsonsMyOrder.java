package com.albertsons.web.staging.page;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;
/**
 * @author Anjulakshmy
 * created on 08/07/2018 | page object class for albertsons Myorders functionality
*/
public class AlbertsonsMyOrder extends ActionValidationLibrary {
    private static AlbertsonsMyOrder albertsonsMyOrder;

    public static Logger logger = Logger.getLogger(AlbertsonsMyOrder.class.getName());

    /******************************************************************************************************************************************
     * Page objects for Albertsons MyOrder Page Class
     *****************************************************************************************************************************************/
    By noOfOrders =By.xpath("//tbody//tr");
    
    By orderNumber =By.xpath("//tbody//tr[2]//td[2]");
    
    By orderDate =By.xpath("//tbody//tr[1]//td[1]");
    
    By shipTo =By.xpath("//tbody//tr[1]//td[3]");
    
    By orderTotal =By.xpath("//tbody//tr[1]//td[4]");
    
    By seller =By.xpath("//tbody//tr[2]//td[5]");
    
    By status =By.xpath("//tbody//tr[2]//td[6]");
    
    By mainRow =By.xpath("//tr[@class='main']");
    
    By viewOrder =By.xpath("//tbody//tr[2]//td[7]");
    
    By productOrder=By.xpath("//select[@class='limiter-options']");
    
    By tenValue =By.xpath("//option[@value='https://staging-marketplace.albertsons.com/sales/order/history/?limit=10']");
    
    By twentyValue =By.xpath("//option[@value='https://staging-marketplace.albertsons.com/sales/order/history/?limit=20']");
    
    By fiftyValue =By.xpath("//option[@value='https://staging-marketplace.albertsons.com/sales/order/history/?limit=50']");
    
    By pagination =By.xpath("//span[text()='Back']");
    
    By textMyorders = By.xpath("//a[text()='My Orders']");
    
    By textAccountInformation = By.xpath("//strong[text()='Account Information']");
    
    By textViewAll = By.xpath("//span[text()='View All']");
    
    By myAccount =By.xpath("//span[@class='inner-heading']//span[text()='My Account']");
	
	By logout =By.xpath("//a[contains(text(),'Sign Out')]");
	
	By txtEmptyOrder =By.xpath("//span[text()='There are currently no orders to display.']");
	
	By txtBack = By.xpath("//span[text()='Back']");
    
   AlbertsonsMyOrder() {

    }
   AlbertsonsMyAccount albersonsMyAccount = new AlbertsonsMyAccount();

    public static AlbertsonsMyOrder getAlbersonsMyOrder() {

        if (AlbertsonsMyOrder.albertsonsMyOrder == null) {
            AlbertsonsMyOrder.albertsonsMyOrder = new AlbertsonsMyOrder();
        }
        return AlbertsonsMyOrder.albertsonsMyOrder;
    }
    
       

	public void getMyorderHistoryVerification() throws Exception {
		try {
			wait(2);
	    	String URLMyorders = driver.getCurrentUrl();
	    	Assert.assertEquals(URLMyorders, "https://staging-marketplace.albertsons.com/sales/order/history/" );
	    	wait(2);
			int count = driver.findElements(By.xpath("//tbody//tr")).size();
//			assertNotEquals(count, 0);
			if (count > 0) {
				this.waitForElementToBeVisible(this.orderDate, "Order Date");
				assertTrue(isElementDisplayed(this.orderDate, "Order Date"));
				this.waitForElementToBeVisible(this.orderNumber, "Order Number");
				assertTrue(isElementDisplayed(this.orderNumber, "Order Number"));
				this.waitForElementToBeVisible(this.shipTo, "Ship to");
				assertTrue(isElementDisplayed(this.shipTo, "Ship to"));
				this.waitForElementToBeVisible(this.orderTotal, "Order total");
				assertTrue(isElementDisplayed(this.noOfOrders, "Order Total"));
				this.waitForElementToBeVisible(this.seller, "Seller");
				assertTrue(isElementDisplayed(this.seller, "Seller"));
				this.waitForElementToBeVisible(this.status, "Status");
				assertTrue(isElementDisplayed(this.status, "Status"));
				this.waitForElementToBeVisible(this.viewOrder, "View Order");
				assertTrue(isElementDisplayed(this.viewOrder, "View Order"));
				int mainCount = driver.findElements(By.xpath("//tbody//tr")).size();
				assertNotEquals(count, 0);
			}
			else{
				assertTrue(isElementDisplayed(this.txtEmptyOrder, "Empty Order"));
			}
			
		} catch (final Exception e) {
			ReporterListener.logFailureMessage("Failed ::to get order history of purchasede products" + e.getMessage());
			throw e;
		}
	}
	
	public void getVerifyPaginationAndOrder() throws Exception {
		try {
			if(isElementDisplayed(this.productOrder, "Product Order")){
			this.scrollToBottom();
			this.waitForElementToBeVisible(this.productOrder, "Product Order");
			this.click(this.productOrder, "Product Order");
			
			this.waitForElementToBeVisible(this.tenValue, "Ten Value");
			assertTrue(isElementDisplayed(this.tenValue, "Ten Value"));
			this.waitForElementToBeVisible(this.twentyValue, "Twenty Value");
			assertTrue(isElementDisplayed(this.twentyValue, "Twenty Value"));
			this.waitForElementToBeVisible(this.fiftyValue, "Thirty Value");
			assertTrue(isElementDisplayed(this.fiftyValue, "Thirty Value"));
			this.waitForElementToBeVisible(this.pagination, "Pagination");
			assertTrue(isElementDisplayed(this.pagination, "Pagination"));
			}
			else{
				this.waitForElementToBeVisible(this.txtEmptyOrder, "Empty Order Message");
				assertTrue(isElementDisplayed(this.txtEmptyOrder, "Empty Order Message"));
			}
			
			
		}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the First and Lastname of the user" + e.getMessage());
	        throw e;
	    }
	}
	public void verifyGoBackFunction() throws Exception {
		try {
			if(isElementDisplayed(this.txtEmptyOrder, "empty Order")){
				
				this.waitForElementToBeClickable(this.txtBack, "Back");
				assertTrue(isElementDisplayed(this.txtBack, "Back"));
				this.click(this.txtBack, "Back");
			
			}
			else
			{
				this.waitForElementToBeVisible(this.pagination, "Pagination");
				assertTrue(isElementDisplayed(this.pagination, "Pagination"));
				this.click(this.pagination, "Pagination");
				wait(3);
				assertTrue(isElementDisplayed(this.textAccountInformation, "Account Information"));
				this.scrollTo(this.textViewAll, "View All");
				wait(3);
				this.waitForElementToBeVisible(this.myAccount, "My Account");
				this.click(this.myAccount, "My Account");
				this.waitForElementToBeVisible(this.logout, "Logout");
				this.click(this.logout, "Logout");
				
			}
		}
		catch (final Exception e) {
	        ReporterListener.logFailureMessage("Failed ::to get the pagination to back page" + e.getMessage());
	        throw e;
	    }
	}
	
    private void turnOffImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnImplicitWaits() {
        Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    
}
