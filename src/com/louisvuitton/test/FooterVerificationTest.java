package com.louisvuitton.test;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.albertsons.web.staging.page.LouisvuittonCheckout;
import com.albertsons.web.staging.page.AlbertsonsGuestAddress;
import com.albertsons.web.staging.page.LouisvuittonHome;
import com.albertsons.web.staging.page.AlbertsonsHomeFooter;
import com.albertsons.web.staging.page.LouisvuittonMinicart;
import com.albertsons.web.staging.page.AlbertsonsMyAccount;
import com.albertsons.web.staging.page.AlbertsonsMyOrder;
import com.albertsons.web.staging.page.LouisvuittonOrderDetails;
import com.albertsons.web.staging.page.LouisvuittonShipping;
import com.albertsons.web.staging.page.LouisvuittonSignIn;

public class FooterVerificationTest extends BaseTest{
	
	protected LouisvuittonHome louisvuittonHomePage;
	protected LouisvuittonSignIn albertsonsSignIn;
	protected LouisvuittonMinicart albertsonsMinicart;
	protected AlbertsonsMyAccount albertsonsMyAccount;
	protected AlbertsonsMyOrder albertsonsMyOrder;
	protected LouisvuittonCheckout albertsonsCheckout;
	protected LouisvuittonShipping albertsonsShipping; 
	protected LouisvuittonOrderDetails albertsonsOrderDetails;
	protected AlbertsonsGuestAddress albertsonsGuestAddress;
	protected AlbertsonsHomeFooter albertsonsFooter;
	public String jsonFileName ="LOUISVUITTON.json";
//	DataLoader dataLoader =new DataLoader();
		
	@BeforeClass
	public void initialize() {
		this.louisvuittonHomePage=LouisvuittonHome.getLouisvuittonHomePage();
	}
	
    @Test(priority=1)
    public void navigateToLouisvuittonHome() throws Exception {
        try {
        	test =extent.createTest("navigateToLouisvuittonHome");
            this.louisvuittonHomePage.navigateToLouisvuittonHome();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=2)
    public void aboutNewsletterLinkVerification() throws Exception {
        try {
        	test =extent.createTest("aboutNewsletterLinkVerification");
            this.louisvuittonHomePage.aboutNewsletterLinkVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=3)
    public void contactLinkVerification() throws Exception {
        try {
        	test =extent.createTest("contactLinkVerification");
            this.louisvuittonHomePage.contactLinkVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=4)
    public void appsLinkVerification() throws Exception {
        try {
        	test =extent.createTest("appsLinkVerification");
            this.louisvuittonHomePage.appsLinkVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=5)
    public void followUsLinkVerification() throws Exception {
        try {
        	test =extent.createTest("followUsLinkVerification");
            this.louisvuittonHomePage.followUsLinkVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=6)
    public void legalAndPrivacyLinkVerification() throws Exception {
        try {
        	test =extent.createTest("legalAndPrivacyLinkVerification");
            this.louisvuittonHomePage.legalAndPrivacyLinkVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=8)
    public void jobsLinkVerification() throws Exception {
        try {
        	test =extent.createTest("jobsLinkVerification");
            this.louisvuittonHomePage.jobsLinkVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test(priority=7)
    public void siteMapLinkVerification() throws Exception {
        try {
        	test =extent.createTest("siteMapLinkVerification");
            this.louisvuittonHomePage.siteMapLinkVerification();
        } catch (final Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    
    
      
 }



