package com.albertsons.web.staging.page;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.louisvuitton.test.DataLoader;
import com.mcfadyen.supportlibraries.ActionValidationLibrary;
import com.mcfadyen.supportlibraries.Base;
import com.mcfadyen.supportlibraries.ReporterListener;

/**
 * @author Anjulakshmy created on 06/13/2019 | page object class for louisvuitton SignIn
 *         functionality
 */
public class LouisvuittonSignIn extends ActionValidationLibrary {
	private static LouisvuittonSignIn louisvuittonSignIn;

	public static Logger logger = Logger.getLogger(LouisvuittonSignIn.class.getName());

	/******************************************************************************************************************************************
	 * Page objects for louisvuitton SignIn Class
	 *****************************************************************************************************************************************/
	By signIn = By.xpath("//strong[text()='Sign In']");

	By userName = By.xpath("//input[@id='loginmylv-bubbleloginFormmylv-bubble']");

	By password = By.xpath("//input[@id='passwordloginFormmylv-bubble']");

	By signingButton = By.xpath("//button[@id='fakemylv-bubbleSignIn']");

	By signUpText = By.xpath("//span[contains(text(),'Welcome, ')]");

	By signUpButton = By.xpath("//button[@id='fakemylv-bubbleSignIn']");

	By textRegisteredCustomers = By.xpath("//strong[text()='Registered Customers']");

	By manageAddress = By.xpath("//span[text()='Manage Addresses']");

	By textInvalidUsername = By.xpath("//div[@id='okta-signin-username-error']");

	By textFirstName = By.xpath("//input[@id='firstname']");

	By textLastName = By.xpath("//input[@id='lastname']");
	
	By myAccount =By.xpath("//strong[text()='My Account']");
	    
	By labelMyAccount =By.xpath("//a[text()='Account']");
	
	By txtHiGuest =By.xpath("//span[contains(text(),'Hi, Guest!')]");
	
	By btnMyAccount =By.xpath("//img[@alt='My Account']");
	
	By txtAccountInformation = By.xpath("//ul[@class='nav items account-nav']//li//a[text()='Account Information']");
	
	private LouisvuittonSignIn() {

	}

	public static LouisvuittonSignIn getLouisvuittonSignIn() {

		if (LouisvuittonSignIn.louisvuittonSignIn == null) {
			LouisvuittonSignIn.louisvuittonSignIn = new LouisvuittonSignIn();
		}
		return LouisvuittonSignIn.louisvuittonSignIn;
	}
	
	public void louisvuittonSignInVerification() throws Exception {
		String firstName=null;String password =null;
		try{
		
		DataLoader dataloader =new DataLoader();
		if((String)dataloader.getTestScenarios("LOUISVUITTON.json","louisvuittonSignInVerification").get(0).get("User_Name")!=null){
			firstName  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","louisvuittonSignInVerification").get(0).get("User_Name");
			password  =(String)dataloader.getTestScenarios("LOUISVUITTON.json","louisvuittonSignInVerification").get(0).get("Password");
			
		}
		driver.findElement(this.userName).clear();
		driver.findElement(this.userName).sendKeys(firstName);
		driver.findElement(this.password).clear();
		driver.findElement(this.password).sendKeys(password);
		this.waitForElementToBeVisible(this.signingButton, "signingButton");
		this.doubleClick(this.signingButton, "signingButton");
		wait(3);
		assertTrue(isElementDisplayed(this.txtAccountInformation, "Account Information"));
	}catch (final Exception e) {
		ReporterListener.logFailureMessage("Failed ::Try to login with valid username" + e.getMessage());
		throw e;
	}
	}
	

	public void firstNameLastNamePhoneVerification() throws Exception {
		try {
			this.waitForElementToBeVisible(this.textFirstName, "First Name");
			assertTrue(isElementDisplayed(this.textFirstName, "First Name"));
			this.waitForElementToBeVisible(this.textLastName, "Last Name");
			assertTrue(isElementDisplayed(this.textLastName, "Last Name"));
		} catch (final Exception e) {
			ReporterListener.logFailureMessage(
					"Failed ::to get the First name Lastname  and phone number of the user" + e.getMessage());
			throw e;
		}
	}

	public void verifyManageAddressLink() throws Exception {
		try {
			this.waitForElementToBeVisible(this.manageAddress, "Manage Address Link");
			assertTrue(isElementDisplayed(this.manageAddress, "Manage Address Link"));
		} catch (final Exception e) {
			ReporterListener.logFailureMessage("Failed ::to get the Manage address link from Address book" + e.getMessage());
			throw e;
		}
	}

	public void getWelcomeGuestName() throws Exception {
		try {
			this.waitForElementToBeVisible(this.signUpText, "signUpText");
			String name = this.getTextFromWebElement(this.signUpText, "Sign Up Text");
		} catch (final Exception e) {
			ReporterListener.logFailureMessage("Failed ::to get the logged in user name" + e.getMessage());
			throw e;
		}
	}

	private void turnOffImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		Base.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

}
